*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com/ 
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***
Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Switch Language
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div/div[3]/ul/li[3]/a/div[2]
    Sleep   2s

Login New Free Vendor
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        xihuan.shop@gmail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   10s

Credit Purchase
    Page Should Contain Element     xpath=//*[@id="peeking-subheader"]/div[3]
    Sleep   2s
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   10s
    Scroll Page To Location         0    750 
    Sleep   2s
    Page Should Contain             10 Credit
    Sleep   2s             
    Page Should Contain Element     css=table.table.table-solid.with-border-outside.fs-normal.table-currentplan-info
    Sleep   2s
    Scroll Page To Location         0    1200
    Sleep   2s
    Page Should Contain             Trial Extension
    Sleep   2s
    Click Element                   css=a.btn.buy-plan-button.tracker_extendtrial.ng-scope
    Sleep   5s
    Scroll Page To Location         0    750 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   12s
    Scroll Page To Location         0    1200
    Sleep   2s   
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/bank-transfer"]
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[1]/div[2]/div/ul/li[3]/label
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[2]/div/button
    Sleep   25s
    Wait Until Page Contains        Thank You for Your Purchase
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   10s
    Page Should Contain Element     css=a.inpagenotif__link.tracker_pendingtransaction.ng-binding
    Sleep   1s
    Scroll Page To Location         0    1200
    Sleep   2s   
    Mouse Over                      css=a.btn.buy-plan-button.tracker_extendtrial.ng-scope.is-disabled
    Sleep   2s
    Page Should Contain Element     css=p.tooltip-box.f-classic.icon-info.ng-scope
    Sleep   2s

Verification Transaction
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   8s
    Scroll Page To Location         0    200
    Sleep   2s  
    Click Element                   css=a.btn.btn-smaller.transstatus__button.ng-scope
    Sleep   5s
    Select Window                   NEW 
    Sleep   2s
    Input Text                      name=paymentGateway                 Bank Negara Indonesia
    Sleep   2s
    Input Text                      name=accountNumber                  1234234243
    Sleep   2s
    Input Text                      name=fullName                       Yeni Marlina
    Sleep   2s
    Input Text                      name=totalAmount                    3465000
    Sleep   2s
    Click Element                   xpath=//*[@id="confirmationForm"]/div[6]/div/div[2]/div/div[1]/div/select
    Sleep   2s
    Click Element                   xpath=//*[@id="confirmationForm"]/div[6]/div/div[2]/div/div[1]/div/select/option[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="confirmationForm"]/div[6]/div/div[2]/div/div[2]/div/select
    Sleep   2s
    Click Element                   xpath=//*[@id="confirmationForm"]/div[6]/div/div[2]/div/div[2]/div/select/option[4]
    Sleep   2s
    Click Element                   xpath=//*[@id="confirmationForm"]/div[6]/div/div[2]/div/div[3]/div/select
    Sleep   2s
    Click Element                   xpath=//*[@id="confirmationForm"]/div[6]/div/div[2]/div/div[3]/div/select/option[2]
    Sleep   2s
    Click Element                   css=button.btn.btn-padding-more.with-border.mr-10.w-200.w-200-rwd
    Sleep   5s
    Click Element                   xpath=//*[@id="rootpaymentconfirmation"]/div[2]/a[1]
    Sleep   8s
    Scroll Page To Location         0    1200
    Sleep   2s   
    Mouse Over                      css=a.btn.buy-plan-button.tracker_extendtrial.ng-scope.is-disabled
    Sleep   2s
    Page Should Contain Element     css=p.tooltip-box.f-classic.icon-info.ng-scope
    Sleep   2s

View Notification Awaiting Verification
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   8s
    Wait Until Page Contains        Pending Transaction
    Sleep   1s
    Wait Until Page Contains        Awaiting Verification
    Sleep   5s

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute Javascript    window.scrollTo(${x_location},${y_location})  

*** Test Cases ***
Free Vendor Credit Purchase
    Launch Browser
    Login New Free Vendor
    Credit Purchase
    Verification Transaction
    View Notification Awaiting Verification