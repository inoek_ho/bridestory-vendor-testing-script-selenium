*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com/ 
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***
Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Switch Language
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div/div[3]/ul/li[3]/a/div[2]
    Sleep   2s

Login Published SG Vendor With Approval Category EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        z@dysco.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   15s

View Vendor Transaction EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   8s
    Wait Until Page Contains        Pending Transaction
    Sleep   1s
    Wait Until Page Contains        Transaction History
    Sleep   5s

View Invoice Before Payment EN
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[1]/table/tbody/tr[1]/td[7]/div
    Sleep   6s
    Select Window                   NEW
    Sleep   2s
    Scroll Page To Location         0    750
    Sleep   2s
    Page Should Contain             Account Name: Bridestory Singapore Pte. Ltd.
    Sleep   2s
    Page Should Contain             Account Number: 695514414001 (SGD)
    Sleep   2s
    Close Window
    Sleep   2s
    Select Window                   title=Buy Packages - Bridestory for Business
    Sleep   2s

View Annual Subscriptions Summary EN
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[1]/a
    Sleep   7s
    Scroll Page To Location         0    600
    Sleep   2s
    Page Should Contain Element     css=table.table.table-solid.with-border-outside.fs-normal.table-annual-subscription
    Sleep   2s

View Pending Transaction From Current Package EN
    Scroll Page To Location         0    0
    Sleep   2s
    Page Should Contain Element     css=.inpagenotif__text
    Sleep   2s
    Click Element                   css=a.inpagenotif__link.tracker_pendingtransaction.ng-binding
    Sleep   10s

Log Out Vendor
    Mouse Over                      xpath=//*[@id="sub-menu"]/li[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[9]/a
    Sleep   3s

Login Published SG Vendor With Approval Category ID
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        z@dysco.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   15s

View Vendor Transaction ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   8s
    Wait Until Page Contains        Transaksi yang Tertunda
    Sleep   1s
    Wait Until Page Contains        Transaction History
    Sleep   5s

View Invoice Before Payment ID
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[1]/table/tbody/tr[1]/td[7]/div
    Sleep   6s
    Select Window                   NEW
    Sleep   2s
    Scroll Page To Location         0    750
    Sleep   2s
    Page Should Contain             Nama Akun: Bridestory Singapore Pte. Ltd.
    Sleep   2s
    Page Should Contain             Nomor Rekening: 695514414001 (SGD)
    Sleep   2s
    Close Window
    Sleep   2s
    Select Window                   title=Buy Packages - Bridestory for Business
    Sleep   2s

View Annual Subscriptions Summary ID
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[1]/a
    Sleep   7s
    Scroll Page To Location         0    600
    Sleep   2s
    Page Should Contain Element     css=table.table.table-solid.with-border-outside.fs-normal.table-annual-subscription
    Sleep   2s

View Pending Transaction From Current Package ID
    Scroll Page To Location         0    0
    Sleep   2s
    Page Should Contain Element     css=.inpagenotif__text
    Sleep   2s
    Click Element                   css=a.inpagenotif__link.tracker_pendingtransaction.ng-binding
    Sleep   10s

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute Javascript    window.scrollTo(${x_location},${y_location})  

*** Test Cases ***
SG Vendor View Invoice and Transaction Summary
    Launch Browser
    Login Published SG Vendor With Approval Category EN 
    View Vendor Transaction EN
    View Invoice Before Payment EN
    View Annual Subscriptions Summary EN
    View Pending Transaction From Current Package EN
    Log Out Vendor
    Switch Language
    Login Published SG Vendor With Approval Category ID
    View Vendor Transaction ID
    View Invoice Before Payment ID
    View Annual Subscriptions Summary ID
    View Pending Transaction From Current Package ID
    Log Out Vendor