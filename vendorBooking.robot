*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com/ 
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***
Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Login Vendor
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        famousejakarta@gmail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   14s

Open Bridestory Pay
	Click Element					xpath=//*[@id="subheader-dashboard"]/ul/li[9]/a
	Sleep	5s
    Scroll Page To Location         0    400 
    Sleep   1s

Create New Quotation
	Click Element					css=a.create-new
	Sleep	2s

Upload Quotation For Client
	Click Element 					xpath=//*[@id="app"]/div/div[2]/div[1]/div[2]/h4/a
	Sleep	2s

Add Recipient From Current List
	Click Element					xpath=//*[@id="app"]/div/div[2]/div[1]/div[3]/div/div[1]/div/div/div/input
	Sleep	2s
	Click Element					xpath=//*[@id="app"]/div/div[2]/div[1]/div[3]/div/div[1]/div/div/div/div/ul/li[1]
	Sleep	2s
	Input Text						css=input#bookingTitle									Jasen and Jeje Wedding
	Sleep	2s
	Click Element					xpath=//*[@id="app"]/div/div[2]/div[1]/div[3]/div/div[3]/div/div/div/div[1]/div/input
	Sleep	1s
	Click Element					css=a.react-datepicker__navigation.react-datepicker__navigation--next
	Sleep	1s 						
	Click Element					css=a.react-datepicker__navigation.react-datepicker__navigation--next
	Sleep	1s 						
	Click Element					xpath=//*[@id="app"]/div/div[2]/div[1]/div[3]/div/div[3]/div/div/div/div[2]/div/div[2]/div[2]/div[5]/div[5]
	Sleep	2s

Payment Details
	Scroll Page To Location         0    400 
    Sleep   1s
	Input Text						name=totalAmount										25000000
	Sleep	2s
	Click Element					css=a.btn-add-payment-schedule
	Sleep	2s			
	Input Text						xpath=//*[@id="app"]/div/div[2]/div[1]/div[4]/div/div[2]/div[2]/div[2]/div[1]/div[1]/div[2]/div/div[3]/div/div[1]/input 	50
	Sleep	2s
	Input Text						xpath=//*[@id="app"]/div/div[2]/div[1]/div[4]/div/div[2]/div[2]/div[2]/div[2]/div[1]/div[2]/div/div[3]/div/div[1]/input 	50
	Sleep	2s
	Click Element					xpath=//*[@id="0"]
	Sleep	2s
	Click Element					css=button.plus
	Sleep	1s
	Click Element					css=button.plus
	Sleep	1s
	Click Element					css=button.plus
	Sleep	1s
	Click Element					css=button.plus
	Sleep	1s
	Click Element 					css=select.unit-field
	Sleep	1s
	Click Element					xpath=//*[@id="setDueDate-field"]/div[1]/div/div[2]/div/div[2]/div/select/option[2]
	Sleep	1s
	Scroll Page To Location         0    400 
    Sleep   1s
    Click Element					css=a.save
    Sleep	2s
    Scroll Page To Location         0    400 
    Sleep   1s
    Click Element					xpath=//*[@id="1"]
    Sleep	2s
	Click Element					css=button.plus
	Sleep	1s
	Click Element					xpath=//*[@id="setDueDate-field"]/div[1]/div/div[2]/div/div[3]/select
	Sleep	1s
	Click Element					xpath=//*[@id="setDueDate-field"]/div[1]/div/div[2]/div/div[3]/select/option[2]
	Sleep	1s
	Scroll Page To Location         0    400 
    Sleep   1s
    Click Element					css=a.save
    Sleep	2s
    Scroll Page To Location         0    400 
    Sleep   1s

Notes
    Scroll Page To Location         0    1500 
    Sleep   1s
    Input Text						xpath=//*[@id="app"]/div/div[2]/div[1]/div[6]/div/div[2]/textarea					Tolong Untuk Segera Di Bayar
    Sleep	2s
    Click Element					xpath=//*[@id="app"]/div/div[2]/div[1]/div[6]/div/div[2]/div/span
    Sleep	2s
    Mouse Over						xpath=//*[@id="app"]/div/div[2]/div[1]/div[6]/div/div[2]/div/span
    Sleep	1s
    Click Element					xpath=//*[@id="app"]/div/div[2]/div[1]/div[6]/div/div[2]/div/span/ul/li/a
    Sleep	2s
    Input Text 						name=templateName																	Pay
    Sleep	2s
    Click Element					css=a.save
    Sleep	2s
    Click Element					xpath=//*[@id="app"]/div/div[2]/div[1]/div[6]/div/div[1]/div/span[2]
    Sleep	2s
    Click Element					xpath=//*[@id="app"]/div/div[2]/div[1]/div[6]/div/div[1]/div/div/div/ul/li[1]
    Sleep	2s

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute Javascript    window.scrollTo(${x_location},${y_location}) 


*** Test Cases ***
Vendor Booking Test
    Launch Browser
    Login Vendor
    Open Bridestory Pay
    Create New Quotation
	Upload Quotation For Client
	Add Recipient From Current List
	Payment Details
	Notes