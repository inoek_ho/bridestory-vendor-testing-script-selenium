*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com/ 
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***
Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Switch Language
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div/div[3]/ul/li[3]/a/div[2]
    Sleep   2s

Login Published SG Vendor With Approval Category EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        zuppa@soup.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   15s

View Vendor Transaction EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   8s
    Wait Until Page Contains        Pending Transaction
    Sleep   1s
    Wait Until Page Contains        Transaction History
    Sleep   4s

View Invoice With BCA VA Number EN
    Scroll Page To Location         0    750
    Sleep   2s
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[2]/table/tbody/tr[4]/td[7]/div
    Sleep   6s
    Select Window                   NEW
    Sleep   2s
    Scroll Page To Location         0    850
    Sleep   2s
    Page Should Contain             Transfer to BCA Virtual Account”
    Sleep   2s
    Page Should Contain             Please enter the Virtual Account number below:
    Sleep   2s
    Close Window
    Sleep   2s
    Select Window                   title=Buy Packages - Bridestory for Business
    Sleep   2s

Log Out Vendor
    Mouse Over                      xpath=//*[@id="sub-menu"]/li[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[9]/a/div[2]
    Sleep   3s

Login Published SG Vendor With Approval Category ID
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        zuppa@soup.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   15s

View Vendor Transaction ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   8s
    Wait Until Page Contains        Transaksi yang Tertunda
    Sleep   1s
    Wait Until Page Contains        Transaction History
    Sleep   4s

View Invoice With BCA VA Number ID
    Scroll Page To Location         0    750
    Sleep   2s
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[2]/table/tbody/tr[4]/td[7]/div
    Sleep   6s
    Select Window                   NEW
    Sleep   2s
    Scroll Page To Location         0    850
    Sleep   2s
    Page Should Contain             "Transfer ke Rek. BCA Virtual Account"
    Sleep   2s
    Page Should Contain             Silakan masukkan nomor Virtual Account:
    Sleep   2s
    Close Window
    Sleep   2s
    Select Window                   title=Buy Packages - Bridestory for Business
    Sleep   2s

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute Javascript    window.scrollTo(${x_location},${y_location}) 

*** Test Cases ***
Vendor View BCA Invoice
    Launch Browser
    Login Published SG Vendor With Approval Category EN
    View Vendor Transaction EN
    View Invoice With BCA VA Number EN
    Log Out Vendor
    Switch Language
    Login Published SG Vendor With Approval Category ID
    View Vendor Transaction ID
    View Invoice With BCA VA Number ID
    Log Out Vendor