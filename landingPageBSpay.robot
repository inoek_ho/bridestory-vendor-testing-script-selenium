*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com/ 
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***
Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Login Vendor 1 EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        saziazavira@gmail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   14s

Login Vendor 2 EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        autotestvendor@mail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   14s

BS Pay Landing Page EN
    Page Should Contain Element     css=.box.border-b.banner-bs-pay.ng-scope
    Sleep   1s
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[9]/a
    Sleep   4s
    Page Should Contain Element     css=.bsp-landing-page.wrapper
    Page Should Contain Element     css=.apply-btn.top
    Sleep   1s
    Scroll Page To Location         0    2000
    Sleep   1s
    Page Should Contain             Wedding Cancellation Protection
    Sleep   1s
    Click Element                   xpath=//*[@id="app"]/div/div[2]/div/div[1]/div[8]/div[3]/div[1]/p/a
    Sleep   2s
    Scroll Page To Location         0    0
    Sleep   1s
    Click Element                   xpath=//*[@id="app"]/div/div[2]/div[3]/div/div/a
    Sleep   2s

Apply BS Pay EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[9]/a
    Sleep   4s
    Click Element                   xpath=//*[@id="app"]/div/div[2]/div/div[1]/div[5]/a
    Sleep   2s
    Click Element                   xpath=//*[@id="app"]/div/div[2]/div[1]/div/div[3]/div[1]/div
    Sleep   2s
    Click Element                   xpath=//*[@id="app"]/div/div[2]/div/div/div[2]/div/a
    Sleep   10s
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[9]/a
    Sleep   4s
    Page Should Contain             Your application for Bridestory Pay is now being reviewed

Log Out Vendor
    Mouse Over                      css=.boxhead__dropdown.boxhead__dropdown--forprofile
    Sleep   2s
    Click Element                   xpath=//*[@id="app"]/div/div[1]/div/div/header/div[1]/div/div[2]/div/div/div[1]/div/div/div/div[10]/a/div[2]
    Sleep   5s

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute Javascript    window.scrollTo(${x_location},${y_location})

*** Test Cases ***
Bridestory Pay Landing Page
    Launch Browser
    Login Vendor 1 EN
    BS Pay Landing Page EN
    Log Out Vendor
    Login Vendor 2 EN
    Apply BS Pay EN
    Log Out Vendor