*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com/ 
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***
Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Switch Language
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div/div[3]/ul/li[3]/a/div[2]
    Sleep   2s

Login Vendor EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        yohanna_trial_only@gmail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   14s

Check Exchange Disc section EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   10s
    Page Should Contain Element     css=.box.with-border.wrapper.center.mt-50.credits__exchange-discount.ng-scope
    Sleep   1s
    Scroll Page To Location         0    515
    Sleep   2s
    Click Element                   css=i.icon.icon.icon-bs-notif-info.f-c-green-strong.f-s-14
    Sleep   4s
    Click Element                   xpath=//*[@id="cboxClose"]
    Sleep   2s

Make Pending Pending Transaction for Test EN
    Scroll Page To Location         0    1300
    Sleep   2s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div/a
    Sleep   15s
    Scroll Page To Location         0    1800 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   22s
    Scroll Page To Location         0    750
    Sleep   1s
    Page Should Contain             Credits Exchange Discount
    Sleep   1s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/bank-transfer"]
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[1]/div[2]/div/ul/li[1]/label
    Sleep   1s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[2]/div/button
    Sleep   45s
    Wait Until Page Contains        Thank You for Your Purchase
    Sleep   1s
    Scroll Page To Location         0    1300
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   10s
    Scroll Page To Location         0    600
    Sleep   2s
    Page Should Contain Element     css=a.btn.buy-plan-button.is-disabled
    Sleep   1s

Cancel Transaction EN
    Scroll Page To Location         0    0
    Sleep   2s
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   5s
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[1]/table/tbody/tr/td[7]/div[1]/i
    Sleep   2s
    Wait Until Page Contains        Are you sure want to cancel this transaction?
    Sleep   1s
    Click Element                   css=button.btn.btn-stronk.with-border.mw-150.ng-binding
    Sleep   4s 

Log Out Vendor
    Mouse Over                      css=li.list-menu.dropdown
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[9]/a
    Sleep   5s

Login Vendor ID
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        yohanna_trial_only@gmail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   14s

Check Exchange Disc section ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   10s
    Page Should Contain Element     css=.box.with-border.wrapper.center.mt-50.credits__exchange-discount.ng-scope
    Sleep   1s
    Scroll Page To Location         0    515
    Sleep   2s
    Click Element                   css=i.icon.icon.icon-bs-notif-info.f-c-green-strong.f-s-14
    Sleep   4s
    Click Element                   xpath=//*[@id="cboxClose"]
    Sleep   2s

Make Pending Pending Transaction for Test ID
    Scroll Page To Location         0    1300
    Sleep   2s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div/a
    Sleep   15s
    Scroll Page To Location         0    1800 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   22s
    Scroll Page To Location         0    750
    Sleep   1s
    Page Should Contain             Credits Exchange Discount
    Sleep   1s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/bank-transfer"]
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[1]/div[2]/div/ul/li[1]/label
    Sleep   1s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[2]/div/button
    Sleep   45s
    Wait Until Page Contains        Thank You for Your Purchase
    Sleep   1s
    Scroll Page To Location         0    1300
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   10s
    Scroll Page To Location         0   600
    Sleep   2s
    Page Should Contain Element     css=a.btn.buy-plan-button.is-disabled
    Sleep   1s

Cancel Transaction ID
    Scroll Page To Location         0    0
    Sleep   2s
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   5s
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[1]/table/tbody/tr/td[7]/div[1]/i
    Sleep   2s
    Wait Until Page Contains        Apakah Anda yakin ingin membatalkan transaksi ini?
    Sleep   1s
    Click Element                   css=button.btn.btn-stronk.with-border.mw-150.ng-binding
    Sleep   4s 

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute Javascript    window.scrollTo(${x_location},${y_location}) 


*** Test Cases ***
Test Installment Payment
    Launch Browser
    Login Vendor EN
    Check Exchange Disc section EN
    Make Pending Pending Transaction for Test EN
    Cancel Transaction EN
    Log Out Vendor
    Switch Language
    Login Vendor ID
    Check Exchange Disc section ID
    Make Pending Pending Transaction for Test ID
    Cancel Transaction ID
    Log Out Vendor