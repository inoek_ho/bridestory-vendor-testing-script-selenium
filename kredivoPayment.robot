*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com/ 
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***
Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Switch Language
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div/div[3]/ul/li[3]/a/div[2]
    Sleep   2s

Login Vendor EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        yukwedding@gmail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   14s

Buy With Kredivo Payment EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   10s
    Scroll Page To Location         0    800
    Sleep   2s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div/a
    Sleep   6s
    Scroll Page To Location         0    1200 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   20s
    Scroll Page To Location         0    750
    Sleep   1s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Page Should Contain             0% Installment Without Credit Card
    Sleep   1s
    Click Element                   xpath=//a[@href="/payment/installment-without-credit-card"]
    Sleep   2s
    Page Should Contain             Kredivo
    Sleep   1s
    Click Element                   xpath=//*[@id="form-0-installment"]/div/div/div[1]/div[2]/div/ul/li/a
    Sleep   2s
    Scroll Page To Location         0    1200 
    Sleep   2s
    Click Element                   xpath=//*[@id="form-0-installment"]/div/form/div/div[2]/div/button
    Sleep   35s
    Element Should Be Disabled      xpath=/html/body/div[2]/div[2]/div[1]/div[1]/form/div[2]/label/input
    Sleep   2s
    Element Should Be Disabled      xpath=/html/body/div[2]/div[2]/div[1]/div[1]/form/div[4]/label/input
    Sleep   1s
    Input Text                      name=inputMobileNumber                      81291891818
    Sleep   2s
    Input Text                      name=inputPassword                          414141
    Sleep   2s
    Click Element                   css=button#btnLogin
    Sleep   8s
    Wait Until Page Contains        MASUKKAN OTP:
    Sleep   1s
    Input Text                      name=otp                                    4567
    Sleep   2s
    Click Element                   css=input#theCheckbox
    Sleep   1s
    Click Element                   css=button#btnConfirmOtp
    Sleep   26s
    Wait Until Page Contains        Your payment has been successfully verified
    Sleep   1s
    Scroll Page To Location         0    1200 
    Sleep   1s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   10

Make Kredivo Pending Payment EN
    Scroll Page To Location         0    800
    Sleep   2s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div/a
    Sleep   6s
    Scroll Page To Location         0    1200 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   20s
    Scroll Page To Location         0    750
    Sleep   1s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Page Should Contain             0% Installment Without Credit Card
    Sleep   1s
    Click Element                   xpath=//a[@href="/payment/installment-without-credit-card"]
    Sleep   2s
    Page Should Contain             Kredivo
    Sleep   1s
    Click Element                   xpath=//*[@id="form-0-installment"]/div/div/div[1]/div[2]/div/ul/li/a
    Sleep   2s
    Scroll Page To Location         0    1200 
    Sleep   2s
    Click Element                   xpath=//*[@id="form-0-installment"]/div/form/div/div[2]/div/button
    Sleep   35s
    Element Should Be Disabled      xpath=/html/body/div[2]/div[2]/div[1]/div[1]/form/div[2]/label/input
    Sleep   2s
    Element Should Be Disabled      xpath=/html/body/div[2]/div[2]/div[1]/div[1]/form/div[4]/label/input
    Sleep   1s
    Input Text                      name=inputMobileNumber                      81291891818
    Sleep   2s
    Input Text                      name=inputPassword                          414141
    Sleep   2s
    Click Element                   css=button#btnLogin
    Sleep   8s
    Wait Until Page Contains        MASUKKAN OTP:
    Sleep   1s
    Click Element                   xpath=/html/body/div[2]/div[1]/div[2]/form/p/a[2]/button
    Sleep   9s
    Wait Until Page Contains        Thank You for Your Purchase
    Sleep   1s
    Scroll Page To Location         0    1200 
    Sleep   2s   
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   10s 

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute Javascript    window.scrollTo(${x_location},${y_location}) 


*** Test Cases ***
Test Installment Payment
    Launch Browser
    Login Vendor EN
    Buy With Kredivo Payment EN
    Make Kredivo Pending Payment EN