*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com/ 
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***
Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Switch Language
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div/div[3]/ul/li[3]/a/div[2]
    Sleep   2s


Login Published SG Vendor With Approval Category EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        xinoin@gmail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   15s

Buy Additional City With Bank Transfer EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   12s
    Scroll Page To Location         0    700 
    Sleep   2s
    Input Text                      xpath=//*[@id="search-form"]/input         Semarang
    Sleep   2s
    Press Key                       xpath=//*[@id="search-form"]/input         \\13
    Sleep   6s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div/a
    Sleep   8s
    Scroll Page To Location         0    1500 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   18s
    Scroll Page To Location         0    1500
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/bank-transfer"]
    Sleep   2s
    Page Should Contain             OCBC Bank Transfer
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[1]/div[2]/div/ul/li/label
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[2]/div/button
    Sleep   18s
    Wait Until Page Contains        ACCOUNT HOLDER NAME Bridestory Singapore Pte. Ltd.
    Sleep   2s
    Scroll Page To Location         0    1500
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   10s

Buy Additional City With PayPal EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   12s
    Scroll Page To Location         0    700 
    Sleep   2s
    Input Text                      xpath=//*[@id="search-form"]/input         Semarang
    Sleep   2s
    Press Key                       xpath=//*[@id="search-form"]/input         \\13
    Sleep   6s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div/a
    Sleep   8s
    Scroll Page To Location         0    1500 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   18s
    Scroll Page To Location         0    1500
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/paypal"]
    Sleep   3s
    Page Should Contain             To complete your payment, please continue by logging in to your Paypal account.
    Sleep   2s
    Click Element                   xpath=//*[@id="braintree-paypal-button"]/img
    Sleep   5s
    Select Window                   NEW 
    Sleep   2s
    Click Element                   xpath=//*[@id="return_url"]
    Sleep   5s
    Select Window                   title=Wedding Vendors - Bridestory.com
    Sleep   2s
    Click Element                   xpath=//*[@id="form-paypal"]/div[2]/div/button
    Sleep   22s
    Page Should Contain             Thank You for Your Purchase
    Sleep   2s
    Scroll Page To Location         0    700 
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   7s 


View Vendor Transaction EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   8s
    Wait Until Page Contains        Pending Transaction
    Sleep   1s
    Wait Until Page Contains        Transaction History
    Sleep   5s

Cancel Transaction EN
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[1]/table/tbody/tr/td[7]/div[1]/i
    Sleep   2s
    Wait Until Page Contains        Are you sure want to cancel this transaction?
    Sleep   1s
    Click Element                   css=button.btn.btn-stronk.with-border.mw-150.ng-binding
    Sleep   2s

Log Out Vendor
    Mouse Over                      xpath=//*[@id="sub-menu"]/li[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[9]/a/div[2]
    Sleep   3s

Login Published SG Vendor With Approval Category ID
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        xinoin@gmail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   15s

Buy Additional City With Bank Transfer ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   12s
    Scroll Page To Location         0    700 
    Sleep   2s
    Input Text                      xpath=//*[@id="search-form"]/input         Semarang
    Sleep   2s
    Press Key                       xpath=//*[@id="search-form"]/input         \\13
    Sleep   6s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div/a
    Sleep   8s
    Scroll Page To Location         0    1500 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   18s
    Scroll Page To Location         0    1500
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/bank-transfer"]
    Sleep   2s
    Page Should Contain             OCBC Bank Transfer
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[1]/div[2]/div/ul/li/label
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[2]/div/button
    Sleep   18s
    Wait Until Page Contains        ACCOUNT HOLDER NAME Bridestory Singapore Pte. Ltd.
    Sleep   2s
    Scroll Page To Location         0    1500
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   10s

Buy Additional City With PayPal ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   12s
    Scroll Page To Location         0    700 
    Sleep   2s
    Input Text                      xpath=//*[@id="search-form"]/input         Semarang
    Sleep   2s
    Press Key                       xpath=//*[@id="search-form"]/input         \\13
    Sleep   6s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div/a
    Sleep   8s
    Scroll Page To Location         0    1500 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   18s
    Scroll Page To Location         0    1500
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/paypal"]
    Sleep   3s
    Page Should Contain             To complete your payment, please continue by logging in to your Paypal account.
    Sleep   2s
    Click Element                   xpath=//*[@id="braintree-paypal-button"]/img
    Sleep   5s
    Select Window                   NEW 
    Sleep   2s
    Click Element                   xpath=//*[@id="return_url"]
    Sleep   5s
    Select Window                   title=Wedding Vendors - Bridestory.com
    Sleep   2s
    Click Element                   xpath=//*[@id="form-paypal"]/div[2]/div/button
    Sleep   22s
    Page Should Contain             Thank You for Your Purchase
    Sleep   2s
    Scroll Page To Location         0    700 
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   7s 


View Vendor Transaction ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   8s
    Wait Until Page Contains        Transaksi yang Tertunda
    Sleep   1s
    Wait Until Page Contains        Transaction History
    Sleep   5s

Cancel Transaction ID
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[1]/table/tbody/tr/td[7]/div[1]/i
    Sleep   2s
    Wait Until Page Contains        Apakah Anda yakin ingin membatalkan transaksi ini?
    Sleep   1s
    Click Element                   css=button.btn.btn-stronk.with-border.mw-150.ng-binding
    Sleep   8s

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute Javascript    window.scrollTo(${x_location},${y_location})  


*** Test Cases ***
SG Vendor Buy Package
    Launch Browser
    Login Published SG Vendor With Approval Category EN
    Buy Additional City With Bank Transfer EN
    View Vendor Transaction EN
    Cancel Transaction EN
    Buy Additional City With Paypal EN
    Log Out Vendor
    Switch Language
    Login Published SG Vendor With Approval Category ID
    Buy Additional City With Bank Transfer ID
    View Vendor Transaction ID
    Cancel Transaction ID
    Buy Additional City With Paypal ID
    Log Out Vendor