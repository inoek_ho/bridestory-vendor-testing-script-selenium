*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com/ 
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***
Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Switch Language
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div/div[3]/ul/li[3]/a/div[2]
    Sleep   2s

Login Hidden Vendor EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        karen_qistynh_putnamescu@tfbnw.net
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   7s

Activation Membership EN
    Wait Until Page Contains        Your Trial has ended
    Sleep   2s
    Click Element                   xpath=/html/body/div[5]/div/div/div/div/span/a/button
    Sleep   7s
    Scroll Page To Location         0    500 
    Sleep   2s
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[3]/table/tbody/tr/td[4]/div/div[2]/div/a
    Sleep   6s
    Scroll Page To Location         0    500 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   12s
    Scroll Page To Location         0    500 
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/credit-card"]
    Sleep   2s
    Input Text                      name=cardNumber                             4811111111111114 
    Sleep   2s
    Click Element                   xpath=//*[@id="expMonth"]
    Sleep   2s
    Click Element                   xpath=//*[@id="expMonth"]/option[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="expYear"]
    Sleep   2s
    Click Element                   xpath=//*[@id="expYear"]/option[4]
    Sleep   2s
    Input Text                      name=cardCVV                                123
    Sleep   2s
    Click Element                   xpath=//*[@id="form-credit-card"]/div[2]/div/button
    Sleep   18s
    Wait Until Page Contains        css=p.info-transfer
    Sleep   1s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   7s
    Close Browser

Login Hidden Vendor ID
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        asas@asa.com.fake
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   7s

Activation Membership ID
    Wait Until Page Contains        Trial Anda sudah berakhir.
    Sleep   2s
    Click Element                   xpath=/html/body/div[5]/div/div/div/div/span/a/button
    Sleep   7s
    Scroll Page To Location         0    500 
    Sleep   2s
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[3]/table/tbody/tr/td[4]/div/div[2]/div/a
    Sleep   6s
    Scroll Page To Location         0    500 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   12s
    Scroll Page To Location         0    500 
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/credit-card"]
    Sleep   2s
    Input Text                      name=cardNumber                             4811111111111114 
    Sleep   2s
    Click Element                   xpath=//*[@id="expMonth"]
    Sleep   2s
    Click Element                   xpath=//*[@id="expMonth"]/option[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="expYear"]
    Sleep   2s
    Click Element                   xpath=//*[@id="expYear"]/option[4]
    Sleep   2s
    Input Text                      name=cardCVV                                123
    Sleep   2s
    Click Element                   xpath=//*[@id="form-credit-card"]/div[2]/div/button
    Sleep   18s
    Wait Until Page Contains        css=p.info-transfer
    Sleep   1s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   7s

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute Javascript    window.scrollTo(${x_location},${y_location})  

*** Test Cases ***
Hidden Vendor Activation
    Launch Browser
    Login Hidden Vendor EN 
    Activation Membership EN
    Launch Browser
    Login Hidden Vendor ID 
    Activation Membership ID