*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com/ 
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***
Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Login Vendor
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        autotestvendor@mail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   14s

Vendor Verification 1
    Location Should Be              url=https://business-staging.bridestory.com/dashboard/essential#/
    Sleep   1s
    Click Element                   xpath=//*[@id="info"]/div/div[2]/div/form[1]/div/div/label[1]/span
    Sleep   2s
    Click Element                   xpath=//*[@id="info"]/div/div[2]/div/form[1]/div/div/label[2]/span
    Sleep   2s
    Click Element                   xpath=//*[@id="info"]/div/div[2]/div/form[1]/p[2]/button
    Sleep   2s
    Click Element                   xpath=//*[@id="info"]/div/div[2]/div/p[2]/a[3]
    Sleep   2s
    Click Element                   xpath=//*[@id="info"]/div/div[2]/div/p[2]/a[3]
    Sleep   2s
    Click Element                   xpath=//*[@id="info"]/div/div[2]/div/p[2]/a[3]
    Sleep   2s
    Click Element                   xpath=//*[@id="info"]/div/div[2]/div/p[2]/a[3]
    Sleep   2s
    Click Element                   xpath=//*[@id="info"]/div/div[2]/div/p[2]/a[3]
    Sleep   2s
    Click Element                   xpath=//*[@id="info"]/div/div[2]/div/p[2]/a[3]
    Sleep   2s
    Click Element                   xpath=//*[@id="info"]/div/div[2]/div/p[2]/a[3]
    Sleep   2s
    Click Element                   xpath=//*[@id="info"]/div/div[2]/div/p[2]/a[3]
    Sleep   2s
    Click Element                   xpath=//*[@id="info"]/div/div[2]/div/form[9]/div/div/label[5]/span
    Sleep   2s
    Click Element                   xpath=//*[@id="info"]/div/div[2]/div/form[9]/p[2]/button
    Sleep   2s
    Click Element                   xpath=//*[@id="info"]/div/div[2]/div/p[2]/a[3]
    Sleep   2s
    Input Text                      name=answer       Our Business Established in 2012
    Sleep   2s
    Click Element                   xpath=//*[@id="info"]/div/div[2]/div/form[10]/p[2]/button
    Sleep   2s
    Input Text                      name=answer          10% after confirmation of contract, 30% after the event, and 60% upon the final delivery
    Sleep   2s
    Click Element                   xpath=//*[@id="info"]/div/div[2]/div/form[9]/p[2]/button
    Sleep   2s
    Input Text                      name=answer          We charge hourly or per event depending on the package picked by the brides
    Sleep   2s
    Click Element                   xpath=//*[@id="info"]/div/div[2]/div/form[8]/p[2]/button
    Sleep   2s
    Input Text                      name=answer          When you choose us for a photo booth hire, you’re guaranteed with quality photography along with the fun!
    Sleep   2s
    Click Element                   xpath=//*[@id="info"]/div/div[2]/div/form[7]/p[2]/button
    Sleep   2s
    Input Text                      name=answer         The most ideal time for a client to contact us would be 1-3 months prior to the wedding
    Sleep   2s
    Click Element                   xpath=//*[@id="info"]/div/div[2]/div/form[6]/p[2]/button
    Sleep   2s
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[2]/ul/li[2]
    Sleep   2s

Vendor Verification 2
    Wait Until Page Contains        Keeping an updated pricelist in your profile will increase the visibility and relevancy of your business in Bridestory.
    Sleep   2s
    Input Text                      name=title          All in One Wedding Car
    Sleep   2s
    Click Element                   xpath=//*[@id="pricelist"]/div/div[2]/form/div[3]/div[1]/select
    Sleep   2s
    Click Element                   xpath=//*[@id="pricelist"]/div/div[2]/form/div[3]/div[1]/select/option[2]
    Sleep   2s
    Input Text                      name=start_3        30000000
    Sleep   2s
    Input Text                      name=item_name      12 hours
    Sleep   2s
    Input Text                      name=description    Wedding Car Rental for 12 hours with driver. Fuel already included
    Sleep   2s
    Click Element                   xpath=//*[@id="pricelist"]/div/div[2]/form/button
    Sleep   2s
    Page Should Contain             Pricelist has been successfully added
    Sleep   4s

Vendor Verification 3
    Wait Until Page Contains        Vendors with more review will be prioritized in the vendor list and search. Invite your clients to give reviews on your profile
    Sleep   2s
    Input Text                      name=name_1         Richard
    Sleep   2s
    Input Text                      name=name_2         Agatha
    Sleep   2s
    Input Text                      name=email_1        richard@mail.com
    Sleep   2s
    Input Text                      name=email2_2       agatha@mail.com
    Sleep   2s
    Click Element                   xpath=//*[@id="review"]/div/div[1]/form/button
    Sleep   2s
    Page Should Contain             Your review request to clients has been sent
    Sleep   4s

Vendor Verification 4
    Wait Until Page Contains        Promote your Bridestory profile on your blog or website!
    Sleep   2s
    Click Element                   xpath=//*[@id="badge-0"]/div/div[2]/button[2]
    Sleep   2s
    Input Text                      xpath=/html/body/div[4]/div/div/div/div/div/div/label[1]/input          mailmail@mail.com
    Sleep   2s
    Click Element                   css=button.btn.with-border.mt-20
    Sleep   3s
    Page Should Contain             The code has been sent successfully!
    Sleep   2s
    Click Element                   xpath=//*[@id="badge"]/div/div[1]/ul/li[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="badge-1"]/div/div[2]/button[1]
    Sleep   2s
    Click Element                   xpath=//*[@id="badge"]/div/div[1]/ul/li[3]
    Sleep   2s
    Click Element                   xpath=//*[@id="badge-2"]/div/div[2]/button[1]
    Sleep   2s
    Click Element                   xpath=//*[@id="badge"]/div/div[1]/ul/li[4]
    Sleep   2s
    Click Element                   xpath=//*[@id="badge-3"]/div/div[2]/button[1]
    Sleep   2s
    Click Element                   xpath=//*[@id="badge"]/div/div[1]/ul/li[5]
    Sleep   2s
    Click Element                   xpath=//*[@id="badge-3"]/div/div[2]/button[1]
    Sleep   2s
    Click Element                   xpath=//*[@id="badge"]/div/div[1]/ul/li[6]
    Sleep   2s
    Click Element                   xpath=//*[@id="badge-3"]/div/div[2]/button[1]
    Sleep   2s
    Click Element                   xpath=//*[@id="badge"]/div/div[1]/ul/li[7]
    Sleep   2s
    Click Element                   xpath=//*[@id="badge-3"]/div/div[2]/button[1]
    Sleep   2s
    Click Element                   xpath=//*[@id="badge"]/div/div[1]/ul/li[8]
    Sleep   2s
    Click Element                   xpath=//*[@id="badge-3"]/div/div[2]/button[1]
    Sleep   2s

Vendor Verifying Content                   
    Wait Until Page Contains        Welcome To Your Dashboard
    Sleep   2s
    Wait Until Page Contains        Your Current Rating Is:
    Sleep   2s
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[2]/div[2]/div[3]/a
    Sleep   2s
    Location Should Be              url=https://business-staging.bridestory.com/dashboard/reviews#/request
    Sleep   1s
    Go Back
    Sleep   2s
    Wait Until Page Contains        Your Progress So Far
    Sleep   2s
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[2]/div[2]/div[4]/a
    Sleep   2s
    Location Should Be              url=https://business-staging.bridestory.com/dashboard/edit#/profile-strength
    Sleep   1s
    Go Back
    Sleep   2s                      
    Wait Until Page Contains        Total Projects
    Sleep   2s
    Wait Until Page Contains        Uploaded
    Sleep   2s
    Wait Until Page Contains        Tagged
    Sleep   2s
    Wait Until Page Contains        Draft
    Sleep   2s
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[2]/div[2]/div[5]/a
    Sleep   2s
    Location Should Be              url=https://business-staging.bridestory.com/dashboard/add-project#/
    Sleep   1s
    Go Back
    Sleep   2s
    Wait Until Page Contains        Profile Views
    Sleep   2s
    Wait Until Page Contains        Contact Clicks
    Sleep   2s
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[2]/div[2]/div[6]/a
    Sleep   2s                  
    Go Back
    Sleep   2s
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[2]/div[2]/div[1]/div/img
    Sleep   2s
    Click Element                   xpath=/html/body/div[5]/div/div/div/a/i
    Sleep   2s             


*** Test Cases ***
Vendor Verifikasi
    Launch Browser
    Login Vendor
    Vendor Verification 1
    Vendor Verification 2
    Vendor Verification 3
    Vendor Verification 4
    Vendor Verifying Content