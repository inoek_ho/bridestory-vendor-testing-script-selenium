*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com/ 
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***
Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Switch Language
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div/div[3]/ul/li[3]/a/div[2]
    Sleep   2s

Login Published Vendor With Approval Category EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        autotestvendor1@mail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   15s

Buy Package Vendor With Credit Card EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   7s
    Scroll Page To Location         0    850
    Sleep   2s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[3]/div[4]/div/a
    Sleep   14s
    Input Text                      xpath=//*[@id="checkout-summary-sticky-container"]/div[2]/div/input         Bali
    Sleep   2s
    Press Key                       xpath=//*[@id="checkout-summary-sticky-container"]/div[2]/div/input         \\13
    Sleep   5s 
    Click Element                   xpath=//*[@id="checkout-summary-sticky-container"]/div[2]/div[2]/div[2]/div[3]/div[2]/input
    Sleep   2s
    Click Element                   xpath=//*[@id="checkout-summary-sticky-container"]/div[2]/div[2]/div[2]/div[3]/div[2]/ul/li[1]
    Sleep   8s                       
    Scroll Page To Location         0    1500
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   18s
    Scroll Page To Location         0    1500
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/credit-card"]
    Sleep   2s
    Input Text						name=cardNumber								4811111111111114 
    Sleep	2s
    Click Element					xpath=//*[@id="expMonth"]
    Sleep	2s
    Click Element					xpath=//*[@id="expMonth"]/option[2]
    Sleep	2s
    Click Element					xpath=//*[@id="expYear"]
    Sleep	2s
    Click Element					xpath=//*[@id="expYear"]/option[4]
    Sleep	2s
    Input Text						name=cardCVV								123
    Sleep	2s
    Click Element					xpath=//*[@id="form-credit-card"]/div[2]/div/button
    Sleep   18s
    Wait Until Page Contains        Thank You for Your Purchase
    Sleep   1s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   7s
    Scroll Page To Location         0    400
    Sleep   2s
    Page Should Contain Element		css=table.table.table-solid.with-border-outside.fs-normal.table-annual-subscription
    Sleep	2s

Log Out Vendor
    Mouse Over                      xpath=//*[@id="sub-menu"]/li[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[9]/a/div[2]
    Sleep   3s

Login Published Vendor With Approval Category ID
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        autotestvendor1@mail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   15s

Buy Package Vendor With Credit Card ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   7s
    Scroll Page To Location         0    850
    Sleep   2s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[3]/div[4]/div/a
    Sleep   14s
    Input Text                      xpath=//*[@id="checkout-summary-sticky-container"]/div[2]/div/input         Bali
    Sleep   2s
    Press Key                       xpath=//*[@id="checkout-summary-sticky-container"]/div[2]/div/input         \\13
    Sleep   5s 
    Click Element                   xpath=//*[@id="checkout-summary-sticky-container"]/div[2]/div[2]/div[2]/div[3]/div[2]/input
    Sleep   2s
    Click Element                   xpath=//*[@id="checkout-summary-sticky-container"]/div[2]/div[2]/div[2]/div[3]/div[2]/ul/li[1]
    Sleep   8s                       
    Scroll Page To Location         0    1500
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   18s
    Scroll Page To Location         0    1500
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/credit-card"]
    Sleep   2s
    Input Text						name=cardNumber								4811111111111114 
    Sleep	2s
    Click Element					xpath=//*[@id="expMonth"]
    Sleep	2s
    Click Element					xpath=//*[@id="expMonth"]/option[2]
    Sleep	2s
    Click Element					xpath=//*[@id="expYear"]
    Sleep	2s
    Click Element					xpath=//*[@id="expYear"]/option[4]
    Sleep	2s
    Input Text						name=cardCVV								123
    Sleep	2s
    Click Element					xpath=//*[@id="form-credit-card"]/div[2]/div/button
    Sleep   18s
    Wait Until Page Contains        Thank You for Your Purchase
    Sleep   1s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   7s
    Scroll Page To Location         0    400
    Sleep   2s
    Page Should Contain Element		css=table.table.table-solid.with-border-outside.fs-normal.table-annual-subscription
    Sleep	2s

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute Javascript    window.scrollTo(${x_location},${y_location}) 

*** Test Cases ***
Vendor Buy Package
    Launch Browser
    Login Published Vendor With Approval Category EN
    Buy Package Vendor With Credit Card EN
    Log Out Vendor
    Switch Language
    Login Published Vendor With Approval Category ID
    Buy Package Vendor With Credit Card ID
    Log Out Vendor