*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***
Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Switch Language
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div/div[3]/ul/li[3]/a/div[2]
    Sleep   2s

Login Vendor EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        autotestvendor@mail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   14s

Edit Profile Vendor EN
    Wait Until Page Contains        Welcome To Your Dashboard
    Sleep   1s
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[2]/a
    Sleep   2s
    Wait Until Page Contains        Edit Profile
    Sleep   2s
    Location Should Be              url=https://business-staging.bridestory.com/dashboard/edit
    Sleep   1s

Side Menu Add Profile Picture EN
    Click Element                   xpath=//*[@id="tabsetName-1"]
    Sleep   2s

Side Menu Business Detail EN
    Click Element                   xpath=//*[@id="tabsetName-2"]
    Sleep   2s
    Element Should Be Disabled      css=input.ng-pristine.ng-valid.ng-valid-required
    Sleep   2s
    Input Text                      name=about                      Our Business is About Event Rental to Make Your Wedding Come True
    Sleep   2s
    Input Text                      name=businessAddress            Jl. Rawa Belong, Kemanggisan, Jakarta Barat
    Sleep   2s
    Element Should Be Disabled      css=select.ng-pristine.ng-valid.ng-valid-required
    Sleep   2s
    Element Should Be Disabled      css=input.ng-pristine.ng-valid.ng-valid-required
    Sleep   2s
    Input Text                      name=zipCode                    777111
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[1]/div[6]/div[1]/div/select
    Sleep   2s          
    Click Element                   xpath=//*[@id="edit-1"]/form[1]/div[6]/div[1]/div/select/option[1]
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[1]/div[6]/h6/a
    Sleep   5s
    Go Back
    Sleep   5s

Side Menu Contacts and Social Media EN
    Click Element                   xpath=//*[@id="tabsetName-3"]
    Sleep   2s
    Input Text                      name=website                    www.rioeventrentalku.com
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/button
    Sleep   5s
    Mouse Over                      xpath=//*[@id="profile"]
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[3]/a/div[2]
    Sleep   5s
    Select Window                   title=RioRio - Rental Vendor di Hong Kong | Bridestory
    Sleep   1s
    Click Element                   xpath=//*[@id="main"]/div[3]/div[1]/div[1]/div[1]/div[3]/div[1]/a
    Sleep   3s
    Select Window                   NEW
    Sleep   1s
    Close Window
    Sleep   1s
    Select Window                   title=RioRio - Rental Vendor di Hong Kong | Bridestory
    Sleep   1s
    Close Window
    Sleep   1s
    Select Window                   title=My Profile - Bridestory for Business
    Sleep   1s
    Input Text                      name=facebook                  https://www.facebook.com/thebridestory
    Sleep   2s
    Input Text                      name=twitter                   TheBrideStory
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/button
    Sleep   2s
    Mouse Over                      xpath=//*[@id="profile"]
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[3]/a/div[2]
    Sleep   5s
    Select Window                   title=RioRio - Rental Vendor di Hong Kong | Bridestory
    Sleep   1s
    Click Element                   css=a.icon-facebook-filled
    Sleep   4s
    Select Window                   NEW
    Sleep   1s
    Close Window
    Sleep   1s
    Select Window                   title=RioRio - Rental Vendor di Hong Kong | Bridestory
    Sleep   1s
    Click Element                   css=a.icon-twitter-filled
    Sleep   4s
    Select Window                   NEW
    Sleep   1s
    Close Window
    Sleep   1s
    Select Window                   title=RioRio - Rental Vendor di Hong Kong | Bridestory
    Sleep   1s
    Close Window
    Sleep   1s
    Select Window                   title=My Profile - Bridestory for Business
    Sleep   1s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/div[8]/div[1]/div/a
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/div[8]/div[1]/ul/li[19]/a
    Sleep   2s
    Input Text                      xpath=//*[@id="edit-1"]/form[2]/div[8]/div[2]/span/input        m.me/rioeventrental
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/button
    Sleep   3s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/div[8]/div[1]/div/a
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/div[8]/div[1]/ul/li[1]/a
    Sleep   2s                  
    Mouse Over                      xpath=//*[@id="edit-1"]/form[2]/div[8]/div[2]/span/input
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/div[8]/div[2]/a/span
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/button
    Sleep   3s
    Input Text                      xpath=//*[@id="edit-1"]/form[2]/div[8]/div[2]/span/input        rioeventrental
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/h6[3]/a
    Sleep   2s
    Input Text                      xpath=//*[@id="edit-1"]/form[2]/div[9]/div[2]/span/input        088876234123
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/h6[3]/a
    Sleep   2s
    Input Text                      xpath=//*[@id="edit-1"]/form[2]/div[10]/div[2]/span/input       rioeventrental
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/h6[3]/a
    Sleep   2s
    Input Text                      xpath=//*[@id="edit-1"]/form[2]/div[11]/div[2]/span/input       rioeventrental
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/h6[3]/a
    Sleep   2s
    Input Text                      xpath=//*[@id="edit-1"]/form[2]/div[12]/div[2]/span/input       53472RIO
    Sleep   2s
    

Side Menu Service Categories and Locations EN
    Click Element                   xpath=//*[@id="tabsetName-4"]
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/div[4]/div[1]/table/tbody/tr[3]/td/h6/a
    Sleep   3s
    Click Element                   xpath=/html/body/div[6]/div/div/form/section[2]/div/select
    Sleep   3s
    Click Element                   xpath=/html/body/div[6]/div/div/form/section[2]/div/select/option[15]
    Sleep   3s
    Click Element                   css=button.btn.btn-stronk.with-border.mt-10.w-210.mb-20
    Sleep   3s
    Click Element                   xpath=//*[@id="edit-1"]/div[4]/div[1]/table/tbody/tr[3]/td[2]/button/span
    Sleep   2s
    Click Element                   css=button.btn.btn-stronk.with-border.mw-150.ng-binding
    Sleep   4s
    Click Element                   xpath=//*[@id="edit-1"]/div[4]/div[2]/table/tbody/tr[2]/td/div/div/button
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/div[4]/div[2]/table/tbody/tr[2]/td/div/label[3]/span
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/div[4]/div[2]/table/tbody/tr[2]/td/div/button[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/div[4]/div[2]/table/tbody/tr[2]/td/div/div/button
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/div[4]/div[2]/table/tbody/tr[2]/td/div/label[1]/span
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/div[4]/div[2]/table/tbody/tr[2]/td/div/button[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/table/tbody/tr[4]/td/h6/a
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/table/tbody/tr[3]/td/form/div/select
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/table/tbody/tr[3]/td/form/div/select/option[96]
    Sleep   2s
    Input Text                      name=planCity                   Medan
    Sleep   3s
    Press Key                       name=planCity                   \\8
    Sleep   3s
    Click Element                   css=.ac-simple.f-classic.f-tcap.f-s-10.ng-scope
    Sleep   2s
    Click Element                   css=button.btn.btn-shorter.btn-smallest.btn-tiny.left
    Sleep   15s
    Click Element                   css=.btn.btn-delete.tiny.right.mt_5
    Sleep   2s
    Wait Until Page Contains        Remove City
    Sleep   2s
    Click Element                   css=button.btn.btn-stronk.with-border.mw-150.ng-binding
    Sleep   15s

Log Out Vendor
    Mouse Over                      css=li.list-menu.dropdown
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[9]/a
    Sleep   3s

Login Vendor ID
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        autotestvendor@mail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   14s

Edit Profile Vendor ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[2]/a
    Sleep   2s
    Wait Until Page Contains        Gambar Profil
    Sleep   2s
    Location Should Be              url=https://business-staging.bridestory.com/id/dashboard/edit
    Sleep   1s

Side Menu Add Profile Picture ID
    Click Element                   xpath=//*[@id="tabsetName-1"]
    Sleep   2s

Side Menu Business Detail ID
    Click Element                   xpath=//*[@id="tabsetName-2"]
    Sleep   2s
    Element Should Be Disabled      css=input.ng-pristine.ng-valid.ng-valid-required
    Sleep   2s
    Input Text                      name=about                      Our Business is About Event Rental to Make Your Wedding Come True
    Sleep   2s
    Input Text                      name=businessAddress            Jl. Rawa Belong, Kemanggisan, Jakarta Barat
    Sleep   2s
    Element Should Be Disabled      css=select.ng-pristine.ng-valid.ng-valid-required
    Sleep   2s
    Element Should Be Disabled      css=input.ng-pristine.ng-valid.ng-valid-required
    Sleep   2s
    Input Text                      name=zipCode                    777111
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[1]/div[6]/div[1]/div/select
    Sleep   2s          
    Click Element                   xpath=//*[@id="edit-1"]/form[1]/div[6]/div[1]/div/select/option[1]
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[1]/div[6]/h6/a
    Sleep   5s
    Go Back
    Sleep   5s

Side Menu Contacts and Social Media ID
    Click Element                   xpath=//*[@id="tabsetName-3"]
    Sleep   2s
    Input Text                      name=website                    www.rioeventrentalku.com
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/button
    Sleep   5s
    Mouse Over                      xpath=//*[@id="profile"]
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[3]/a/div[2]
    Sleep   5s
    Select Window                   title=RioRio - Rental Vendor di Hong Kong | Bridestory
    Sleep   1s
    Click Element                   xpath=//*[@id="main"]/div[3]/div[1]/div[1]/div[1]/div[3]/div[1]/a
    Sleep   3s
    Select Window                   NEW
    Sleep   1s
    Close Window
    Sleep   1s
    Select Window                   title=RioRio - Rental Vendor di Hong Kong | Bridestory
    Sleep   1s
    Close Window
    Sleep   1s
    Select Window                   title=My Profile - Bridestory for Business
    Sleep   1s
    Input Text                      name=facebook                  https://www.facebook.com/thebridestory
    Sleep   2s
    Input Text                      name=twitter                   TheBrideStory
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/button
    Sleep   2s
    Mouse Over                      xpath=//*[@id="profile"]
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[3]/a/div[2]
    Sleep   5s
    Select Window                   title=RioRio - Rental Vendor di Hong Kong | Bridestory
    Sleep   1s
    Click Element                   css=a.icon-facebook-filled
    Sleep   4s
    Select Window                   NEW
    Sleep   1s
    Close Window
    Sleep   1s
    Select Window                   title=RioRio - Rental Vendor di Hong Kong | Bridestory
    Sleep   1s
    Click Element                   css=a.icon-twitter-filled
    Sleep   4s
    Select Window                   NEW
    Sleep   1s
    Close Window
    Sleep   1s
    Select Window                   title=RioRio - Rental Vendor di Hong Kong | Bridestory
    Sleep   1s
    Close Window
    Sleep   1s
    Select Window                   title=My Profile - Bridestory for Business
    Sleep   1s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/div[8]/div[1]/div/a
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/div[8]/div[1]/ul/li[19]/a
    Sleep   2s
    Input Text                      xpath=//*[@id="edit-1"]/form[2]/div[8]/div[2]/span/input        m.me/rioeventrental
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/button
    Sleep   3s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/div[8]/div[1]/div/a
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/div[8]/div[1]/ul/li[1]/a
    Sleep   2s                  
    Mouse Over                      xpath=//*[@id="edit-1"]/form[2]/div[8]/div[2]/span/input
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/div[8]/div[2]/a/span
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/button
    Sleep   3s
    Input Text                      xpath=//*[@id="edit-1"]/form[2]/div[8]/div[2]/span/input        rioeventrental
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/h6[3]/a
    Sleep   2s
    Input Text                      xpath=//*[@id="edit-1"]/form[2]/div[9]/div[2]/span/input        088876234123
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/h6[3]/a
    Sleep   2s
    Input Text                      xpath=//*[@id="edit-1"]/form[2]/div[10]/div[2]/span/input       rioeventrental
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/h6[3]/a
    Sleep   2s
    Input Text                      xpath=//*[@id="edit-1"]/form[2]/div[11]/div[2]/span/input       rioeventrental
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/form[2]/h6[3]/a
    Sleep   2s
    Input Text                      xpath=//*[@id="edit-1"]/form[2]/div[12]/div[2]/span/input       53472RIO
    Sleep   2s
    

Side Menu Service Categories and Locations ID
    Click Element                   xpath=//*[@id="tabsetName-4"]
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/div[4]/div[1]/table/tbody/tr[3]/td/h6/a
    Sleep   3s
    Click Element                   xpath=/html/body/div[5]/div/div/form/section[2]/div/select
    Sleep   3s
    Click Element                   xpath=/html/body/div[5]/div/div/form/section[2]/div/select/option[5]
    Sleep   3s
    Click Element                   css=button.btn.btn-stronk.with-border.mt-10.w-210.mb-20
    Sleep   3s
    Click Element                   xpath=//*[@id="edit-1"]/div[4]/div[1]/table/tbody/tr[3]/td[2]/button/span
    Sleep   2s
    Click Element                   css=button.btn.btn-stronk.with-border.mw-150.ng-binding
    Sleep   4s
    Click Element                   xpath=//*[@id="edit-1"]/div[4]/div[2]/table/tbody/tr[2]/td/div/div/button
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/div[4]/div[2]/table/tbody/tr[2]/td/div/label[3]/span
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/div[4]/div[2]/table/tbody/tr[2]/td/div/button[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/div[4]/div[2]/table/tbody/tr[2]/td/div/div/button
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/div[4]/div[2]/table/tbody/tr[2]/td/div/label[1]/span
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/div[4]/div[2]/table/tbody/tr[2]/td/div/button[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/table/tbody/tr[4]/td/h6/a
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/table/tbody/tr[3]/td/form/div/select
    Sleep   2s
    Click Element                   xpath=//*[@id="edit-1"]/table/tbody/tr[3]/td/form/div/select/option[96]
    Sleep   2s
    Input Text                      name=planCity                   Medan
    Sleep   3s
    Press Key                       name=planCity                   \\8
    Sleep   3s
    Click Element                   css=.ac-simple.f-classic.f-tcap.f-s-10.ng-scope
    Sleep   2s
    Click Element                   css=button.btn.btn-shorter.btn-smallest.btn-tiny.left
    Sleep   15s
    Click Element                   css=.btn.btn-delete.tiny.right.mt_5
    Sleep   2s
    Wait Until Page Contains        Hapus Kota
    Sleep   2s
    Click Element                   css=button.btn.btn-stronk.with-border.mw-150.ng-binding
    Sleep   15s

*** Test Cases ***
Vendor Verifikasi
    Launch Browser
    Login Vendor EN
    Edit Profile Vendor EN
    Side Menu Add Profile Picture EN
    Side Menu Business Detail EN
    Side Menu Contacts and Social Media EN
    Side Menu Service Categories and Locations EN
    Log Out Vendor
    Switch Language
    Login Vendor ID
    Edit Profile Vendor ID
    Side Menu Add Profile Picture ID
    Side Menu Business Detail ID
    Side Menu Contacts and Social Media ID
    Side Menu Service Categories and Locations ID
    Log Out Vendor