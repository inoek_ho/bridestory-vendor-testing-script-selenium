*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com/ 
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***
Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Switch Language
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div/div[3]/ul/li[3]/a/div[2]
    Sleep   2s

Login Hidden Vendor EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        yanifriendsmusic@yahoo.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   10s

Hidden Vendor Home EN
    Page Should Contain Element     css=.popup-box.w-660.center.ng-scope
    Sleep   2s
    Click Element                   css=i.icon-close-big
    Sleep   2s
    Page Should Contain             Inactive
    Sleep   1s
    Page Should Contain Element     css=div#subheader-notification
    Sleep   1s
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[6]/a
    Sleep   5s
    Page Should Contain Element     css=.popup-box.w-660.center.ng-scope
    Sleep   2s
    Click Element                   css=i.icon-close-big
    Sleep   2s
    Page Should Contain Element     css=.inpageoverlay__body
    Sleep   2s
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[5]/a
    Sleep   5s
    Page Should Contain Element     css=.popup-box.w-660.center.ng-scope
    Sleep   2s
    Click Element                   css=i.icon-close-big
    Sleep   2s
    Page Should Contain Element     css=.inpageoverlay__body
    Sleep   2s
    Click Element                   css=i.icon.bs-font-icon-inbox-outline
    Sleep   4s
    Click Element                   xpath=//*[@id="app"]/div/div/div/div[2]/div[1]/div[1]/div[3]/div/a
    Sleep   2s
    Page Should Contain Element     css=.no-access-message
    Sleep   2s

Log Out Hidden Vendor EN
    Mouse Over                      xpath=//*[@id="app"]/div/div/div/div[1]/div/header/div[1]/div/div[2]/div/div/div[1]/div/a/div[1]
    Sleep   2s
    Click Element                   xpath=//*[@id="app"]/div/div/div/div[1]/div/header/div[1]/div/div[2]/div/div/div[1]/div/div/div/div[7]/a/div[2]
    Sleep   4s               

Login Vendor 0 Credit EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        yolkatart@gmail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   10s

Log Out 0 Credit Vendor EN
    Mouse Over                      xpath=//*[@id="app"]/div/div/div/div[1]/div/header/div[1]/div/div[2]/div/div/div[1]/div/a/div[1]
    Sleep   2s
    Click Element                   xpath=//*[@id="app"]/div/div/div/div[1]/div/header/div[1]/div/div[2]/div/div/div[1]/div/div/div/div[10]/a/div[2]
    Sleep   4s     

Vendor 0 Credit Home EN
    Page Should Contain Element     css=div#subheader-zerocredit
    Sleep   1s
    Page Should Contain Element     css=header#header2
    Sleep   1s
    Page Should Contain Element     css=div#subheader-dashboard
    Sleep   2s
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[6]/a
    Sleep   5s
    Page Should Contain Element     css=.inpageoverlay__body
    Sleep   2s
    Click Element                   css=i.icon.bs-font-icon-inbox-outline
    Sleep   4s
    Click Element                   xpath=//*[@id="app"]/div/div/div/div[2]/div[1]/div[1]/div[3]/div/a
    Sleep   2s
    Page Should Contain Element     css=.no-access-message
    Sleep   2s

PH Free Vendor Login EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        garry_villaverde18@yahoo.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   10s

PH Free Vendor Transaction EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s
    Page Should Contain Element     css=div#subscription-tutorial
    Sleep   1s
    Scroll Page To Location         0    1350 
    Sleep   2s
    Click Element                   css=a.btn.buy-plan-button.tracker_extendtrial.ng-scope
    Sleep   4s
    Scroll Page To Location         0    1200 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   12s
    Scroll Page To Location         0    1200 
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/bank-transfer"]
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[1]/div[2]/div/ul/li/label
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[2]/div/button
    Sleep   15s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   6s

See Term and Condition Trial Vendor EN
    Scroll Page To Location         0    2000 
    Sleep   2s
    Click Element                   xpath=/html/body/footer/div[2]/ul/li[2]/a
    Sleep   4s
    Scroll Page To Location         0    850 
    Sleep   2s
    Page Should Contain             BRIDESTORY TRIAL ACCOUNT
    Sleep   1s
    Go Back
    Sleep   5s

Log Out PH Free Vendor EN
    Mouse Over                      xpath=//*[@id="sub-menu"]/li[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[9]/a/div[2]
    Sleep   4s   

Paying Vendor Login EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        polaristudio@yahoo.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   10s

Make Sure Paying Vendor Cannot Buy Credits EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s
    Scroll Page To Location         0    950 
    Sleep   2s
    Page Should Not Contain         Trial Extension
    Sleep   1s

Log Out Paying Vendor EN
    Mouse Over                      xpath=//*[@id="sub-menu"]/li[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[9]/a/div[2]
    Sleep   4s  

Login 5 Credits Vendor EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        renei@weddingcampaign.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   10s

See Notification 5 Credit Left EN
    Page Should Contain Element     css=.inpagenotif.inpagenotif--alert.inpagenotif--subheader.inpagenotif--withbutton.ng-scope
    Sleep   1s
    Page Should Contain             You're almost out of your credits. Don't lose business opportunities,
    Sleep   1s

Log Out 5 Credits Vendor EN
    Mouse Over                      xpath=//*[@id="sub-menu"]/li[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[9]/a/div[2]
    Sleep   4s  

Login White Listed Vendor EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        yumikatsuraindonesia@gmail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   10s

See White Listed Message EN
    Click Element                   css=i.icon.bs-font-icon-inbox-outline
    Sleep   3s                  

Log Out White Listed Vendor EN
    Mouse Over                      css=.boxhead__dropdown.boxhead__dropdown--forprofile
    Sleep   2s
    Click Element                   xpath=//*[@id="app"]/div/div/div/div[1]/div/header/div[1]/div/div[2]/div/div/div[1]/div/div/div/div[10]/a/div[2]
    Sleep   4s 

Cancel Transaction EN
    Scroll Page To Location         0    250 
    Sleep   2s      
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[1]/table/tbody/tr/td[7]/div[1]/i
    Sleep   2s
    Wait Until Page Contains        Are you sure want to cancel this transaction?
    Sleep   1s
    Click Element                   css=button.btn.btn-stronk.with-border.mw-150.ng-binding
    Sleep   8s

View Vendor Transaction EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   8s
    Wait Until Page Contains        Pending Transaction
    Sleep   1s
    Wait Until Page Contains        Transaction History
    Sleep   5s

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute Javascript    window.scrollTo(${x_location},${y_location}) 

Login Hidden Vendor ID
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        yanifriendsmusic@yahoo.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   10s

Hidden Vendor Home ID
    Page Should Contain Element     css=.popup-box.w-660.center.ng-scope
    Sleep   2s
    Click Element                   css=i.icon-close-big
    Sleep   2s
    Page Should Contain             Non-Aktif
    Sleep   1s
    Page Should Contain Element     css=div#subheader-notification
    Sleep   1s
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[6]/a
    Sleep   5s
    Page Should Contain Element     css=.popup-box.w-660.center.ng-scope
    Sleep   2s
    Click Element                   css=i.icon-close-big
    Sleep   2s
    Page Should Contain Element     css=.inpageoverlay__body
    Sleep   2s
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[5]/a
    Sleep   5s
    Page Should Contain Element     css=.popup-box.w-660.center.ng-scope
    Sleep   2s
    Click Element                   css=i.icon-close-big
    Sleep   2s
    Page Should Contain Element     css=.inpageoverlay__body
    Sleep   2s
    Click Element                   css=i.icon.bs-font-icon-inbox-outline
    Sleep   4s
    Click Element                   xpath=//*[@id="app"]/div/div/div/div[2]/div[1]/div[1]/div[3]/div/a
    Sleep   2s
    Page Should Contain Element     css=.no-access-message
    Sleep   2s

Log Out Hidden Vendor ID
    Mouse Over                      xpath=//*[@id="app"]/div/div/div/div[1]/div/header/div[1]/div/div[2]/div/div/div[1]/div/a/div[1]
    Sleep   2s
    Click Element                   xpath=//*[@id="app"]/div/div/div/div[1]/div/header/div[1]/div/div[2]/div/div/div[1]/div/div/div/div[7]/a/div[2]
    Sleep   4s               

Login Vendor 0 Credit ID
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        yolkatart@gmail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   10s

Log Out 0 Credit Vendor ID
    Mouse Over                      css=.boxhead__dropdown.boxhead__dropdown--forprofile
    Sleep   2s
    Click Element                   xpath=//*[@id="app"]/div/div/div/div[1]/div/header/div[1]/div/div[2]/div/div/div[1]/div/div/div/div[10]/a/div[2]
    Sleep   4s     

Vendor 0 Credit Home ID
    Page Should Contain Element     css=div#subheader-zerocredit
    Sleep   1s
    Page Should Contain Element     css=header#header2
    Sleep   1s
    Page Should Contain Element     css=div#subheader-dashboard
    Sleep   2s
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[6]/a
    Sleep   5s
    Page Should Contain Element     css=.inpageoverlay__body
    Sleep   2s
    Click Element                   css=i.icon.bs-font-icon-inbox-outline
    Sleep   4s
    Click Element                   xpath=//*[@id="app"]/div/div/div/div[2]/div[1]/div[1]/div[3]/div/a
    Sleep   2s
    Page Should Contain Element     css=.no-access-message
    Sleep   2s

PH Free Vendor Login ID
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        garry_villaverde18@yahoo.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   10s

PH Free Vendor Transaction ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s
    Page Should Contain Element     css=div#subscription-tutorial
    Sleep   1s
    Scroll Page To Location         0    1200 
    Sleep   2s
    Click Element                   css=a.btn.buy-plan-button.tracker_extendtrial.ng-scope
    Sleep   4s
    Scroll Page To Location         0    1200 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   12s
    Scroll Page To Location         0    1200 
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/bank-transfer"]
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[1]/div[2]/div/ul/li/label
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[2]/div/button
    Sleep   15s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   6s

See Term and Condition Trial Vendor ID
    Scroll Page To Location         0    2000 
    Sleep   2s
    Click Element                   xpath=/html/body/footer/div[2]/ul/li[2]/a
    Sleep   4s
    Scroll Page To Location         0    850 
    Sleep   2s
    Page Should Contain             Akun "Trial" Bridestory
    Sleep   1s
    Go Back
    Sleep   5s

Log Out PH Free Vendor ID
    Mouse Over                      xpath=//*[@id="sub-menu"]/li[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[9]/a/div[2]
    Sleep   4s   

Paying Vendor Login ID
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        polaristudio@yahoo.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   10s

Make Sure Paying Vendor Cannot Buy Credits ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s
    Scroll Page To Location         0    950 
    Sleep   2s
    Page Should Not Contain         Perpanjangan Trial
    Sleep   1s

Log Out Paying Vendor ID
    Mouse Over                      xpath=//*[@id="sub-menu"]/li[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[9]/a/div[2]
    Sleep   4s  

Login 5 Credits Vendor ID
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        renei@weddingcampaign.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   10s

See Notification 5 Credit Left ID
    Page Should Contain Element     css=.inpagenotif.inpagenotif--alert.inpagenotif--subheader.inpagenotif--withbutton.ng-scope
    Sleep   1s
    Page Should Contain             Kredit Anda hampir habis, segera dapatkan kredit tak terbatas dengan
    Sleep   1s

Log Out 5 Credits Vendor ID
    Mouse Over                      xpath=//*[@id="sub-menu"]/li[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[9]/a/div[2]
    Sleep   4s  

Login White Listed Vendor ID
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        yumikatsuraindonesia@gmail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   10s

See White Listed Message ID
    Click Element                   css=i.icon.bs-font-icon-inbox-outline
    Sleep   3s                  

Log Out White Listed Vendor ID
    Mouse Over                      css=.boxhead__dropdown.boxhead__dropdown--forprofile
    Sleep   2s
    Click Element                   xpath=//*[@id="app"]/div/div/div/div[1]/div/header/div[1]/div/div[2]/div/div/div[1]/div/div/div/div[10]/a/div[2]
    Sleep   4s 

Cancel Transaction ID
    Scroll Page To Location         0    250 
    Sleep   2s      
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[1]/table/tbody/tr/td[7]/div[1]/i
    Sleep   2s
    Wait Until Page Contains        Apakah Anda yakin ingin membatalkan transaksi ini?
    Sleep   1s
    Click Element                   css=button.btn.btn-stronk.with-border.mw-150.ng-binding
    Sleep   8s

View Vendor Transaction ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   8s
    Wait Until Page Contains        Transaksi yang Tertunda
    Sleep   1s
    Wait Until Page Contains        Transaction History
    Sleep   5s

*** Test Case ***
Hidden and 0 Credit Vendor Test Case
    Launch Browser
    Login Hidden Vendor EN
    Hidden Vendor Home EN
    Log Out Hidden Vendor EN
    Login Vendor 0 Credit EN
    Vendor 0 Credit Home EN
    Log Out 0 Credit Vendor EN
    PH Free Vendor Login EN
    PH Free Vendor Transaction EN
    See Term and Condition Trial Vendor EN
    View Vendor Transaction EN
    Cancel Transaction EN
    Log Out PH Free Vendor EN
    Paying Vendor Login EN
    Make Sure Paying Vendor Cannot Buy Credits EN
    Log Out Paying Vendor EN
    Login 5 Credits Vendor EN
    See Notification 5 Credit Left EN
    Log Out 5 Credits Vendor EN
    Login White Listed Vendor EN
    See White Listed Message EN
    Log Out White Listed Vendor EN
    Switch Language
    Login Hidden Vendor ID
    Hidden Vendor Home ID
    Log Out Hidden Vendor ID
    Login Vendor 0 Credit ID
    Vendor 0 Credit Home ID
    Log Out 0 Credit Vendor ID
    PH Free Vendor Login ID
    PH Free Vendor Transaction ID
    See Term and Condition Trial Vendor ID
    View Vendor Transaction ID
    Cancel Transaction ID
    Log Out PH Free Vendor ID
    Paying Vendor Login ID
    Make Sure Paying Vendor Cannot Buy Credits ID
    Log Out Paying Vendor ID
    Login 5 Credits Vendor ID
    See Notification 5 Credit Left ID
    Log Out 5 Credits Vendor ID
    Login White Listed Vendor ID
    See White Listed Message ID
    Log Out White Listed Vendor ID