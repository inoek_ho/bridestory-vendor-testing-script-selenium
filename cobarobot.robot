*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com/ 
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***
Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Switch Language
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div/div[3]/ul/li[3]/a/div[2]
    Sleep   2s

Login Hidden Vendor EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        karen_qistynh_putnamescu@tfbnw.net
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   7s

Activation Membership EN
    Wait Until Page Contains        Your Trial has ended
    Sleep   2s
    Click Element                   css=i.icon-close-big
    Sleep   7s
    Close Browser

Activation Membership ID
    Wait Until Page Contains        Your Trial has ended
    Sleep   2s
    Click Element                   css=i.icon-close-big
    Sleep   7s

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute Javascript    window.scrollTo(${x_location},${y_location})  

*** Test Cases ***
Hidden Vendor Activation
    Launch Browser
    Login Hidden Vendor EN
    Activation Membership EN
    Launch Browser
    Login Hidden Vendor EN
    Activation Membership ID
