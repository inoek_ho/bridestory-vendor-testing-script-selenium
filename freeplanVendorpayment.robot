*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com/ 
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***
Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Switch Language
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div/div[3]/ul/li[3]/a/div[2]
    Sleep   2s

Login Published Freeplan Vendor With Approval Category EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        mentaripagi_30@yahoo.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   14s


Subscribe Primary City With Bank Transfer EN
	Click Element					xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
	Sleep	12s
	Page Should Contain				Current Packages
	Sleep	2s
	Scroll Page To Location         0    300 
    Sleep   2s
    Click Element					xpath=//*[@id="topmost-wrapper"]/div[2]/table/tbody/tr/td[4]/div/div[2]/div/a
    Sleep	6s
	Scroll Page To Location         0    650 	
	Sleep	2s
	Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   18s
	Scroll Page To Location         0    600 	
	Sleep	2s
	Click Element					xpath=/html/body/ng-view/div/div[2]/a/button
	Sleep	2s
	Click Element					xpath=//a[@href="/payment/bank-transfer"]
	Sleep	2s
	Click Element					xpath=//*[@id="form-bank-transfer"]/div[1]/div[2]/div/ul/li[1]/label
	Sleep	2s
	Click Element					xpath=//*[@id="form-bank-transfer"]/div[2]/div/button
	Sleep	20s
	Scroll Page To Location         0    800 	
	Sleep	2s
	Click Element					xpath=/html/body/ng-view/div/div[2]/button
	Sleep	7s

View Vendor Transaction EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   8s
    Wait Until Page Contains        Pending Transaction
    Sleep   1s
    Wait Until Page Contains        Transaction History
    Sleep   5s

Cancel Transaction EN
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[1]/table/tbody/tr/td[7]/div[1]/i
    Sleep   2s
    Wait Until Page Contains        Are you sure want to cancel this transaction?
    Sleep   1s
    Click Element                   css=button.btn.btn-stronk.with-border.mw-150.ng-binding
    Sleep   8s

Subscribe Additional City With Bank Transfer EN
	Click Element					xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
	Sleep	12s
	Scroll Page To Location         0    500 
    Sleep   2s
    Input Text                      xpath=//*[@id="search-form"]/input         Yogyakarta
    Sleep   2s
    Press Key                       xpath=//*[@id="search-form"]/input         \\13
    Sleep   6s
    Click Element					xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div/a
    Sleep	8s
   	Scroll Page To Location         0    750
    Sleep   2s
	Click Element					css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep	12s
    Scroll Page To Location         0    1500
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/bank-transfer"]
    Sleep   2s
    Click Element					xpath=//*[@id="form-bank-transfer"]/div[1]/div[2]/div/ul/li[1]/label
    Sleep	2s
    Click Element					xpath=//*[@id="form-bank-transfer"]/div[2]/div/button
    Sleep	20s
    Scroll Page To Location         0    1500
    Sleep   2s
    Click Element					xpath=/html/body/ng-view/div/div[2]/button
    Sleep	8s

Log Out Vendor
    Mouse Over                      xpath=//*[@id="sub-menu"]/li[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[9]/a/div[2]
    Sleep   3s

Login Published Freeplan Vendor With Approval Category ID
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        mentaripagi_30@yahoo.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   14s


Subscribe Primary City With Bank Transfer ID
	Click Element					xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
	Sleep	12s
	Page Should Contain				Paket Saat Ini
	Sleep	2s
	Scroll Page To Location         0    300 
    Sleep   2s
    Click Element					xpath=//*[@id="topmost-wrapper"]/div[2]/table/tbody/tr/td[4]/div/div[2]/div/a
    Sleep	6s
	Scroll Page To Location         0    650 	
	Sleep	2s
	Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   18s
	Scroll Page To Location         0    600 	
	Sleep	2s
	Click Element					xpath=/html/body/ng-view/div/div[2]/a/button
	Sleep	2s
	Click Element					xpath=//a[@href="/payment/bank-transfer"]
	Sleep	2s
	Click Element					xpath=//*[@id="form-bank-transfer"]/div[1]/div[2]/div/ul/li[1]/label
	Sleep	2s
	Click Element					xpath=//*[@id="form-bank-transfer"]/div[2]/div/button
	Sleep	18s
	Scroll Page To Location         0    2000 	
	Sleep	2s
	Click Element					xpath=/html/body/ng-view/div/div[2]/button
	Sleep	7s

View Vendor Transaction ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   8s
    Wait Until Page Contains        Transaksi yang Tertunda
    Sleep   1s
    Wait Until Page Contains        Transaction History
    Sleep   5s

Cancel Transaction ID
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[1]/table/tbody/tr/td[7]/div[1]/i
    Sleep   2s
    Wait Until Page Contains        Apakah Anda yakin ingin membatalkan transaksi ini?
    Sleep   1s
    Click Element                   css=button.btn.btn-stronk.with-border.mw-150.ng-binding
    Sleep   8s

Subscribe Additional City With Bank Transfer ID
	Click Element					xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
	Sleep	12s
	Scroll Page To Location         0    500 
    Sleep   2s
    Input Text                      xpath=//*[@id="search-form"]/input         Yogyakarta
    Sleep   2s
    Press Key                       xpath=//*[@id="search-form"]/input         \\13
    Sleep   6s
    Click Element					xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div/a
    Sleep	8s
   	Scroll Page To Location         0    750
    Sleep   2s
	Click Element					css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep	12s
    Scroll Page To Location         0    1500
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/bank-transfer"]
    Sleep   2s
    Click Element					xpath=//*[@id="form-bank-transfer"]/div[1]/div[2]/div/ul/li[1]/label
    Sleep	2s
    Click Element					xpath=//*[@id="form-bank-transfer"]/div[2]/div/button
    Sleep	20s
    Scroll Page To Location         0    2000
    Sleep   2s
    Click Element					xpath=/html/body/ng-view/div/div[2]/button
    Sleep	8s

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute Javascript    window.scrollTo(${x_location},${y_location})  


*** Test Cases ***
Freeplan Vendor Payment
    Launch Browser
	Login Published Freeplan Vendor With Approval Category EN
	Subscribe Primary City With Bank Transfer EN
	View Vendor Transaction EN 
	Cancel Transaction EN
	Subscribe Additional City With Bank Transfer EN
	View Vendor Transaction EN
	Cancel Transaction EN
	Log Out Vendor
	Switch Language
	Login Published Freeplan Vendor With Approval Category ID
	Subscribe Primary City With Bank Transfer ID
	View Vendor Transaction ID
	Cancel Transaction ID
	Subscribe Additional City With Bank Transfer ID
	View Vendor Transaction ID
	Cancel Transaction ID
	Log Out Vendor