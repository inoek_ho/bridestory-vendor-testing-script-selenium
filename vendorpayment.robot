*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com?sss
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***
Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Switch Language
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div/div[3]/ul/li[3]/a/div[2]
    Sleep   2s

Login Unpublised Vendor EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        muditadevi@gmail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   15s


Make Sure Unpublised Vendor Cannot Click Buy Package EN
    Scroll Page To Location         0    1500 
    Sleep   2s 
    Click Element                   xpath=/html/body/div[4]/section/div/div[5]/p[1]/a
    Sleep   15s
    Page Should Contain Element     css=span.unpublish.ng-scope
    Sleep   2s


Logout Unpublised Vendor EN
    Mouse Over                      xpath=//*[@id="profile"]
    Sleep   1s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[8]/a
    Sleep   5s                

Login Published Vendor With Approval Category EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        faketest@zysco.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   15s

Buy Package Vendor With Bank Transfer BCA EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   7s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div
    Sleep   4s                            
    Input Text                      xpath=//*[@id="checkout-summary-sticky-container"]/div[2]/div/input         Semarang
    Sleep   2s
    Press Key                       xpath=//*[@id="checkout-summary-sticky-container"]/div[2]/div/input         \\13
    Sleep   5s 
    Click Element                   xpath=//*[@id="checkout-summary-sticky-container"]/div[2]/div[2]/div[2]/div[3]/div[2]/input
    Sleep   3s
    Click Element                   xpath=//*[@id="checkout-summary-sticky-container"]/div[2]/div[2]/div[2]/div[3]/div[2]/ul/li[1]
    Sleep   8s     
    Scroll Page To Location         0    1550 
    Sleep   2s                 
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   18s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//*[@id="method-bank-transfer"]/a
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[1]/div[2]/div/ul/li[1]/label
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[2]/div/button
    Sleep   40s
    Wait Until Page Contains        Thank You for Your Purchase
    Sleep   2s
    Click Element                   xpath=//*[@id="confirm-virtual-transfer"]/div/div[1]/h3
    Sleep   2s
    Click Element                   xpath=//*[@id="confirm-virtual-transfer"]/div/div[2]/h3
    Sleep   2s
    Click Element                   xpath=//*[@id="confirm-virtual-transfer"]/div/div[3]/h3
    Sleep   2s
    Page Should Not Contain         Bridestory BCA Account
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   10s
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   7s
    Page Should Not Contain         css=a.btn.btn-smaller.transstatus__button.ng-scope
    Sleep   2s

Cancel Transaction EN
    Scroll Page To Location         0    490 
    Sleep   2s      
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[1]/table/tbody/tr/td[7]/div[1]/i
    Sleep   2s
    Wait Until Page Contains        Are you sure want to cancel this transaction?
    Sleep   1s
    Click Element                   css=button.btn.btn-stronk.with-border.mw-150.ng-binding
    Sleep   8s

View Vendor Transaction EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   8s
    Wait Until Page Contains        Pending Transaction
    Sleep   1s
    Wait Until Page Contains        Transaction History
    Sleep   5s

Extends Package Plan With BNI EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s  
    Scroll Page To Location         0    550 
    Sleep   2s      
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[1]/table/tbody/tr[6]/td/a
    Sleep   8s
    Scroll Page To Location         0    1500 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   18s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   3s
    Click Element                   xpath=//*[@id="method-bank-transfer"]/a
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[1]/div[2]/div/ul/li[3]/label
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[2]/div/button
    Sleep   40s
    Wait Until Page Contains        Thank You for Your Purchase
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   10s

Confirm Payment EN
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   8s
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[1]/table/tbody/tr/td[7]/a
    Sleep   5s
    Select Window                   url=https://business-staging.bridestory.com/dashboard/confirm-payment/7997061520417117#/
    Sleep   2s
    Close Window
    Sleep   2s
    Select Window                   title=Buy Packages - Bridestory for Business
    Sleep   2s

View Invoice After Payment EN
    Scroll Page To Location         0    650 
    Sleep   2s  
    Click Element                   xpath=//a[@href="https://business-staging.bridestory.com/dashboard/invoice/7997061466395012"]
    Sleep   6s
    Select Window                   url=https://business-staging.bridestory.com/dashboard/invoice/7997061466395012#/
    Sleep   2s
    Page Should Contain             VAT (PPN 10%)
    Sleep   3s
    Close Window                    
    Sleep   2s
    Select Window                   title=Buy Packages - Bridestory for Business
    Sleep   2s

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute Javascript    window.scrollTo(${x_location},${y_location})

Log Out Vendor
    Mouse Over                      css=li.list-menu.dropdown
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[9]/a/div[2]
    Sleep   3s

Login Unpublised Vendor ID
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        muditadevi@gmail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   15s


Make Sure Unpublised Vendor Cannot Click Buy Package ID
    Scroll Page To Location         0    1500 
    Sleep   2s 
    Click Element                   xpath=/html/body/div[4]/section/div/div[5]/p[1]/a
    Sleep   15s
    Page Should Contain Element     css=span.unpublish.ng-scope
    Sleep   2s


Logout Unpublised Vendor ID
    Mouse Over                      xpath=//*[@id="profile"]
    Sleep   1s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[8]/a
    Sleep   5s                

Login Published Vendor With Approval Category ID
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        faketest@zysco.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   15s

Buy Package Vendor With Bank Transfer BCA ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   7s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div
    Sleep   4s                            
    Input Text                      xpath=//*[@id="checkout-summary-sticky-container"]/div[2]/div/input         Semarang
    Sleep   2s
    Press Key                       xpath=//*[@id="checkout-summary-sticky-container"]/div[2]/div/input         \\13
    Sleep   5s 
    Click Element                   xpath=//*[@id="checkout-summary-sticky-container"]/div[2]/div[2]/div[2]/div[3]/div[2]/input
    Sleep   3s
    Click Element                   xpath=//*[@id="checkout-summary-sticky-container"]/div[2]/div[2]/div[2]/div[3]/div[2]/ul/li[1]
    Sleep   8s     
    Scroll Page To Location         0    1550 
    Sleep   2s                 
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   18s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//*[@id="method-bank-transfer"]/a
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[1]/div[2]/div/ul/li[1]/label
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[2]/div/button
    Sleep   40s
    Wait Until Page Contains        Thank You for Your Purchase
    Sleep   2s
    Click Element                   xpath=//*[@id="confirm-virtual-transfer"]/div/div[1]/h3
    Sleep   2s
    Click Element                   xpath=//*[@id="confirm-virtual-transfer"]/div/div[2]/h3
    Sleep   2s
    Click Element                   xpath=//*[@id="confirm-virtual-transfer"]/div/div[3]/h3
    Sleep   2s
    Page Should Not Contain         Bridestory BCA Account
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   10s
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   7s
    Page Should Not Contain         css=a.btn.btn-smaller.transstatus__button.ng-scope
    Sleep   2s

Cancel Transaction ID
    Scroll Page To Location         0    490 
    Sleep   2s      
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[1]/table/tbody/tr/td[7]/div[1]/i
    Sleep   2s
    Wait Until Page Contains        Apakah Anda yakin ingin membatalkan transaksi ini?
    Sleep   1s
    Click Element                   css=button.btn.btn-stronk.with-border.mw-150.ng-binding
    Sleep   8s

View Vendor Transaction ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   8s
    Wait Until Page Contains        Transaksi yang Tertunda
    Sleep   1s
    Wait Until Page Contains        Transaction History
    Sleep   5s

Extends Package Plan With BNI ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s  
    Scroll Page To Location         0    550 
    Sleep   2s      
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[1]/table/tbody/tr[6]/td/a
    Sleep   8s
    Scroll Page To Location         0    1500 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   18s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   3s
    Click Element                   xpath=//*[@id="method-bank-transfer"]/a
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[1]/div[2]/div/ul/li[3]/label
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[2]/div/button
    Sleep   40s
    Wait Until Page Contains        Thank You for Your Purchase
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   10s

Confirm Payment ID
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   8s
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[1]/table/tbody/tr/td[7]/a
    Sleep   5s
    Select Window                   url=https://business-staging.bridestory.com/dashboard/confirm-payment/7997061520417117#/
    Sleep   2s
    Close Window
    Sleep   2s
    Select Window                   title=Buy Packages - Bridestory for Business
    Sleep   2s

View Invoice After Payment ID
    Scroll Page To Location         0    650 
    Sleep   2s  
    Click Element                   xpath=//a[@href="https://business-staging.bridestory.com/id/dashboard/invoice/7997061466395012/"]
    Sleep   6s
    Select Window                   url=https://business-staging.bridestory.com/dashboard/invoice/7997061466395012#/
    Sleep   2s
    Page Should Contain             VAT (PPN 10%)
    Sleep   3s
    Close Window                    
    Sleep   2s
    Select Window                   title=Buy Packages - Bridestory for Business
    Sleep   2s  

*** Test Cases ***
Vendor Buy Package
    Launch Browser
    Login Unpublised Vendor EN
    Make Sure Unpublised Vendor Cannot Click Buy Package EN
    Logout Unpublised Vendor EN
    Login Published Vendor With Approval Category EN
    Buy Package Vendor With Bank Transfer BCA EN
    View Vendor Transaction EN
    Cancel Transaction EN
    Extends Package Plan With BNI EN
    View Vendor Transaction EN
    Cancel Transaction EN

    View Vendor Transaction EN
    View Invoice After Payment EN
    Log Out Vendor
    Switch Language
    Login Unpublised Vendor ID
    Make Sure Unpublised Vendor Cannot Click Buy Package ID
    Logout Unpublised Vendor ID
    Login Published Vendor With Approval Category ID
    Buy Package Vendor With Bank Transfer BCA ID
    View Vendor Transaction ID
    Cancel Transaction ID
    Extends Package Plan With BNI ID
    View Vendor Transaction ID
    Cancel Transaction ID
    
    View Vendor Transaction ID
    View Invoice After Payment ID
    Log Out Vendor