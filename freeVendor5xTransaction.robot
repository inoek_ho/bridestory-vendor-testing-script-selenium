*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com/ 
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***
Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Switch Language
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div/div[3]/ul/li[3]/a/div[2]
    Sleep   2s

Login New SG Free Vendor EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        truetolove.sg@gmail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   10s

Credit Purchase 1x EN
    Page Should Contain Element     xpath=//*[@id="peeking-subheader"]/div[3]
    Sleep   2s
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   10s
    Scroll Page To Location         0    750 
    Sleep   2s
    Page Should Contain             10 Credit
    Sleep   2s             
    Page Should Contain Element     css=table.table.table-solid.with-border-outside.fs-normal.table-currentplan-info
    Sleep   2s
    Scroll Page To Location         0    1200
    Sleep   2s
    Page Should Contain             Trial Extension
    Sleep   2s
    Click Element                   css=a.btn.buy-plan-button.tracker_extendtrial.ng-scope
    Sleep   5s
    Scroll Page To Location         0    750 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   12s
    Scroll Page To Location         0    1200
    Sleep   2s   
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/paypal"]
    Sleep   3s
    Page Should Contain             To complete your payment, please continue by logging in to your Paypal account.
    Sleep   2s
    Click Element                   xpath=//*[@id="braintree-paypal-button"]/img
    Sleep   5s
    Select Window                   NEW 
    Sleep   2s
    Click Element                   xpath=//*[@id="return_url"]
    Sleep   5s
    Select Window                   title=Wedding Vendors - Bridestory.com
    Sleep   2s
    Click Element                   xpath=//*[@id="form-paypal"]/div[2]/div/button
    Sleep   22s
    Page Should Contain             Thank You for Your Purchase
    Sleep   2s
    Scroll Page To Location         0    700 
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   10s 

Credit Purchase 2x EN
    Scroll Page To Location         0    750 
    Sleep   2s
    Click Element                   css=a.btn.buy-plan-button.tracker_extendtrial.ng-scope
    Sleep   5s
    Scroll Page To Location         0    750 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   12s
    Scroll Page To Location         0    1200
    Sleep   2s   
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/paypal"]
    Sleep   3s
    Page Should Contain             To complete your payment, please continue by logging in to your Paypal account.
    Sleep   2s
    Click Element                   xpath=//*[@id="braintree-paypal-button"]/img
    Sleep   5s
    Select Window                   NEW 
    Sleep   2s
    Click Element                   xpath=//*[@id="return_url"]
    Sleep   5s
    Select Window                   title=Wedding Vendors - Bridestory.com
    Sleep   2s
    Click Element                   xpath=//*[@id="form-paypal"]/div[2]/div/button
    Sleep   22s
    Page Should Contain             Thank You for Your Purchase
    Sleep   2s
    Scroll Page To Location         0    700 
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   5s 

Credit Purchase 3x EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   10s
    Scroll Page To Location         0    750 
    Sleep   2s
    Click Element                   css=a.btn.buy-plan-button.tracker_extendtrial.ng-scope
    Sleep   5s
    Scroll Page To Location         0    750 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   12s
    Scroll Page To Location         0    1200
    Sleep   2s   
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/paypal"]
    Sleep   3s
    Page Should Contain             To complete your payment, please continue by logging in to your Paypal account.
    Sleep   2s
    Click Element                   xpath=//*[@id="braintree-paypal-button"]/img
    Sleep   5s
    Select Window                   NEW 
    Sleep   2s
    Click Element                   xpath=//*[@id="return_url"]
    Sleep   5s
    Select Window                   title=Wedding Vendors - Bridestory.com
    Sleep   2s
    Click Element                   xpath=//*[@id="form-paypal"]/div[2]/div/button
    Sleep   22s
    Page Should Contain             Thank You for Your Purchase
    Sleep   2s
    Scroll Page To Location         0    700 
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   5s 

Credit Purchase 4x EN
    Scroll Page To Location         0    750 
    Sleep   2s
    Click Element                   css=a.btn.buy-plan-button.tracker_extendtrial.ng-scope
    Sleep   5s
    Scroll Page To Location         0    750 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   12s
    Scroll Page To Location         0    1200
    Sleep   2s   
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/paypal"]
    Sleep   3s
    Page Should Contain             To complete your payment, please continue by logging in to your Paypal account.
    Sleep   2s
    Click Element                   xpath=//*[@id="braintree-paypal-button"]/img
    Sleep   5s
    Select Window                   NEW 
    Sleep   2s
    Click Element                   xpath=//*[@id="return_url"]
    Sleep   5s
    Select Window                   title=Wedding Vendors - Bridestory.com
    Sleep   2s
    Click Element                   xpath=//*[@id="form-paypal"]/div[2]/div/button
    Sleep   22s
    Page Should Contain             Thank You for Your Purchase
    Sleep   2s
    Scroll Page To Location         0    700 
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   5s

Credit Purchase 5x EN
    Scroll Page To Location         0    800 
    Sleep   2s
    Click Element                   css=a.btn.buy-plan-button.tracker_extendtrial.ng-scope
    Sleep   5s
    Scroll Page To Location         0    750 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   12s
    Scroll Page To Location         0    1200
    Sleep   2s   
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/paypal"]
    Sleep   3s
    Page Should Contain             To complete your payment, please continue by logging in to your Paypal account.
    Sleep   2s
    Click Element                   xpath=//*[@id="braintree-paypal-button"]/img
    Sleep   5s
    Select Window                   NEW 
    Sleep   2s
    Click Element                   xpath=//*[@id="return_url"]
    Sleep   5s
    Select Window                   title=Wedding Vendors - Bridestory.com
    Sleep   2s
    Click Element                   xpath=//*[@id="form-paypal"]/div[2]/div/button
    Sleep   22s
    Page Should Contain             Your payment has been successfully verified
    Sleep   2s
    Scroll Page To Location         0    700 
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   5s

Make Sure Cant Extend Again EN
    Scroll Page To Location         0    800 
    Sleep   2s
    Page Should Contain Element     css=a.btn.buy-plan-button.tracker_extendtrial.ng-scope.is-disabled
    Sleep   2s

View Vendor Transaction EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   8s
    Scroll Page To Location         0    650 
    Sleep   2s
    Wait Until Page Contains        Pending Transaction
    Sleep   1s
    Wait Until Page Contains        Transaction History
    Sleep   1s

View Invoice EN
    Scroll Page To Location         0    1550 
    Sleep   2s
    Click Element                   css=.transstatus__icon.transstatus__icon--doc
    Sleep   5s
    Select Window                   NEW
    Sleep   1s
    Scroll Page To Location         0    650 
    Sleep   2s
    Close Window
    Sleep   2s
    Select Window                   title=Buy Packages - Bridestory for Business
    Sleep   2s

Close The Browser
    Close Browser

Login New SG Free Vendor ID
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        contact@theaislebridal.com.sg
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   10s

Credit Purchase 1x ID
    Page Should Contain Element     xpath=//*[@id="peeking-subheader"]/div[3]
    Sleep   2s
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   10s
    Scroll Page To Location         0    750 
    Sleep   2s
    Page Should Contain             10 Kredit
    Sleep   2s             
    Page Should Contain Element     css=table.table.table-solid.with-border-outside.fs-normal.table-currentplan-info
    Sleep   2s
    Scroll Page To Location         0    1200
    Sleep   2s
    Page Should Contain             Perpanjangan Trial
    Sleep   2s
    Click Element                   css=a.btn.buy-plan-button.tracker_extendtrial.ng-scope
    Sleep   5s
    Scroll Page To Location         0    750 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   12s
    Scroll Page To Location         0    1200
    Sleep   2s   
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/paypal"]
    Sleep   3s
    Page Should Contain             To complete your payment, please continue by logging in to your Paypal account.
    Sleep   2s
    Click Element                   xpath=//*[@id="braintree-paypal-button"]/img
    Sleep   5s
    Select Window                   NEW 
    Sleep   2s
    Click Element                   xpath=//*[@id="return_url"]
    Sleep   5s
    Select Window                   title=Wedding Vendors - Bridestory.com
    Sleep   2s
    Click Element                   xpath=//*[@id="form-paypal"]/div[2]/div/button
    Sleep   22s
    Page Should Contain             Thank You for Your Purchase
    Sleep   2s
    Scroll Page To Location         0    700 
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   10s 

Credit Purchase 2x ID
    Scroll Page To Location         0    750 
    Sleep   2s
    Click Element                   css=a.btn.buy-plan-button.tracker_extendtrial.ng-scope
    Sleep   5s
    Scroll Page To Location         0    750 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   12s
    Scroll Page To Location         0    1200
    Sleep   2s   
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/paypal"]
    Sleep   3s
    Page Should Contain             To complete your payment, please continue by logging in to your Paypal account.
    Sleep   2s
    Click Element                   xpath=//*[@id="braintree-paypal-button"]/img
    Sleep   5s
    Select Window                   NEW 
    Sleep   2s
    Click Element                   xpath=//*[@id="return_url"]
    Sleep   5s
    Select Window                   title=Wedding Vendors - Bridestory.com
    Sleep   2s
    Click Element                   xpath=//*[@id="form-paypal"]/div[2]/div/button
    Sleep   22s
    Page Should Contain             Thank You for Your Purchase
    Sleep   2s
    Scroll Page To Location         0    700 
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   5s 

Credit Purchase 3x ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   10s
    Scroll Page To Location         0    750 
    Sleep   2s
    Click Element                   css=a.btn.buy-plan-button.tracker_extendtrial.ng-scope
    Sleep   5s
    Scroll Page To Location         0    750 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   12s
    Scroll Page To Location         0    1200
    Sleep   2s   
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/paypal"]
    Sleep   3s
    Page Should Contain             To complete your payment, please continue by logging in to your Paypal account.
    Sleep   2s
    Click Element                   xpath=//*[@id="braintree-paypal-button"]/img
    Sleep   5s
    Select Window                   NEW 
    Sleep   2s
    Click Element                   xpath=//*[@id="return_url"]
    Sleep   5s
    Select Window                   title=Wedding Vendors - Bridestory.com
    Sleep   2s
    Click Element                   xpath=//*[@id="form-paypal"]/div[2]/div/button
    Sleep   22s
    Page Should Contain             Thank You for Your Purchase
    Sleep   2s
    Scroll Page To Location         0    700 
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   5s 

Credit Purchase 4x ID
    Scroll Page To Location         0    750 
    Sleep   2s
    Click Element                   css=a.btn.buy-plan-button.tracker_extendtrial.ng-scope
    Sleep   5s
    Scroll Page To Location         0    750 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   12s
    Scroll Page To Location         0    1200
    Sleep   2s   
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/paypal"]
    Sleep   3s
    Page Should Contain             To complete your payment, please continue by logging in to your Paypal account.
    Sleep   2s
    Click Element                   xpath=//*[@id="braintree-paypal-button"]/img
    Sleep   5s
    Select Window                   NEW 
    Sleep   2s
    Click Element                   xpath=//*[@id="return_url"]
    Sleep   5s
    Select Window                   title=Wedding Vendors - Bridestory.com
    Sleep   2s
    Click Element                   xpath=//*[@id="form-paypal"]/div[2]/div/button
    Sleep   22s
    Page Should Contain             Thank You for Your Purchase
    Sleep   2s
    Scroll Page To Location         0    700 
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   5s

Credit Purchase 5x ID
    Scroll Page To Location         0    800 
    Sleep   2s
    Click Element                   css=a.btn.buy-plan-button.tracker_extendtrial.ng-scope
    Sleep   5s
    Scroll Page To Location         0    750 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   12s
    Scroll Page To Location         0    1200
    Sleep   2s   
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/paypal"]
    Sleep   3s
    Page Should Contain             To complete your payment, please continue by logging in to your Paypal account.
    Sleep   2s
    Click Element                   xpath=//*[@id="braintree-paypal-button"]/img
    Sleep   5s
    Select Window                   NEW 
    Sleep   2s
    Click Element                   xpath=//*[@id="return_url"]
    Sleep   5s
    Select Window                   title=Wedding Vendors - Bridestory.com
    Sleep   2s
    Click Element                   xpath=//*[@id="form-paypal"]/div[2]/div/button
    Sleep   22s
    Page Should Contain             Your payment has been successfully verified
    Sleep   2s
    Scroll Page To Location         0    700 
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   5s

View Vendor Transaction ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   8s
    Scroll Page To Location         0    650 
    Sleep   2s
    Wait Until Page Contains        Transaksi yang Tertunda
    Sleep   1s
    Wait Until Page Contains        Transaction History
    Sleep   1s

View Invoice ID
    Scroll Page To Location         0    1550 
    Sleep   2s
    Click Element                   css=.transstatus__icon.transstatus__icon--doc
    Sleep   5s
    Select Window                   NEW
    Sleep   1s
    Scroll Page To Location         0    650 
    Sleep   2s
    Close Window
    Sleep   2s
    Select Window                   title=Buy Packages - Bridestory for Business
    Sleep   2s

Make Sure Cant Extend Again ID
    Scroll Page To Location         0    800 
    Sleep   2s
    Page Should Contain Element     css=a.btn.buy-plan-button.tracker_extendtrial.ng-scope.is-disabled
    Sleep   2s

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute Javascript    window.scrollTo(${x_location},${y_location}) 

*** Test Cases ***
5x Transaction Purchase
    Launch Browser
    Login New SG Free Vendor EN
    Credit Purchase 1x EN
    Credit Purchase 2x EN
    Credit Purchase 3x EN
    Credit Purchase 4x EN
    Credit Purchase 5x EN
    Make Sure Cant Extend Again EN
    View Vendor Transaction EN
    View Invoice EN
    Close The Browser
    Launch Browser
    Switch Language
    Login New SG Free Vendor ID
    Credit Purchase 1x ID
    Credit Purchase 2x ID
    Credit Purchase 3x ID
    Credit Purchase 4x ID
    Credit Purchase 5x ID
    Make Sure Cant Extend Again ID
    View Vendor Transaction ID
    View Invoice ID