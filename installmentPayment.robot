*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com/ 
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***
Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Switch Language
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div/div[3]/ul/li[3]/a/div[2]
    Sleep   2s

Login Vendor EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        francodafer@gmail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   15s

Using BNI Installment Payment EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   10s
    Scroll Page To Location         0    1000
    Sleep   2s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div/a
    Sleep   7s
    Scroll Page To Location         0    500 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   22s
    Scroll Page To Location         0    750
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/installment-with-credit-card"]
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/installment-with-credit-card/bni-credit-card"]
    Sleep   2s
    Input Text                      name=cardNumber                         5264221038874659
    Sleep   2s
    Click Element                   xpath=//*[@id="expMonth"]
    Sleep   2s
    Click Element                   xpath=//*[@id="expMonth"]/option[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="expYear"]
    Sleep   2s
    Click Element                   xpath=//*[@id="expYear"]/option[4]
    Sleep   2s
    Input Text                      name=cardCVV                                123
    Sleep   2s
    Click Element                   xpath=//*[@id="form-0-installment"]/div/form/div/div[2]/div/button
    Sleep   6s
    Select Frame                    xpath=//*[@id="cboxLoadedContent"]/iframe
    Sleep   2s
    Input Text                      css=input#PaRes                             112233
    Sleep   2s
    Click Element                   css=button.btn.btn-sm.btn-success
    Sleep   18s
    Wait Until Page Contains        Thank You for Your Purchase
    Sleep   1s
    Scroll Page To Location         0    1500
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   7s

Using Mandiri Installment Payment EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   10s
    Scroll Page To Location         0    1000
    Sleep   2s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div/a
    Sleep   7s
    Scroll Page To Location         0    500 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   22s
    Scroll Page To Location         0    750
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/installment-with-credit-card"]
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/installment-with-credit-card/mandiri-credit-card"]
    Sleep   2s
    Input Text                      name=cardNumber                         4617006959746656
    Sleep   2s
    Click Element                   xpath=//*[@id="expMonth"]
    Sleep   2s
    Click Element                   xpath=//*[@id="expMonth"]/option[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="expYear"]
    Sleep   2s
    Click Element                   xpath=//*[@id="expYear"]/option[4]
    Sleep   2s
    Input Text                      name=cardCVV                                123
    Sleep   2s
    Click Element                   xpath=//*[@id="form-0-installment"]/div/form/div/div[2]/div/button
    Sleep   6s
    Select Frame                    xpath=//*[@id="cboxLoadedContent"]/iframe
    Sleep   2s
    Input Text                      css=input#PaRes                             112233
    Sleep   2s
    Click Element                   css=button.btn.btn-sm.btn-success
    Sleep   18s
    Wait Until Page Contains        Thank You for Your Purchase
    Sleep   1s
    Scroll Page To Location         0    1500
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   7s

Using BCA Installment Payment EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   10s
    Scroll Page To Location         0    1000
    Sleep   2s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div/a
    Sleep   7s
    Scroll Page To Location         0    500 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   22s
    Scroll Page To Location         0    750
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/installment-with-credit-card"]
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/installment-with-credit-card/bca-credit-card"]
    Sleep   2s
    Input Text                      name=cardNumber                             4773776057051650
    Sleep   2s
    Click Element                   xpath=//*[@id="expMonth"]
    Sleep   2s
    Click Element                   xpath=//*[@id="expMonth"]/option[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="expYear"]
    Sleep   2s
    Click Element                   xpath=//*[@id="expYear"]/option[4]
    Sleep   2s
    Input Text                      name=cardCVV                                123
    Sleep   2s
    Click Element                   xpath=//*[@id="form-0-installment"]/div/form/div/div[2]/div/button
    Sleep   6s
    Select Frame                    xpath=//*[@id="cboxLoadedContent"]/iframe
    Sleep   2s
    Input Text                      css=input#PaRes                             112233
    Sleep   2s
    Click Element                   css=button.btn.btn-sm.btn-success
    Sleep   18s
    Wait Until Page Contains        Thank You for Your Purchase
    Sleep   1s
    Scroll Page To Location         0    1500
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   8s

Check Invoice BCA Transaction EN
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   6s
    Scroll Page To Location         0    750
    Sleep   2s
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[2]/table/tbody/tr[1]/td[7]/div
    Sleep   4s
    Select Window                   NEW
    Sleep   1s
    Close Window
    Sleep   1s
    Select Window                   title=Buy Packages - Bridestory for Business
    Sleep   1s

Log Out Vendor
    Mouse Over                      css=li.list-menu.dropdown
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[9]/a
    Sleep   5s

Login Vendor ID
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        francodafer@gmail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   15s

Using BNI Installment Payment ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   10s
    Scroll Page To Location         0    1000
    Sleep   2s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div/a
    Sleep   7s
    Scroll Page To Location         0    500 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   22s
    Scroll Page To Location         0    750
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/installment-with-credit-card"]
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/installment-with-credit-card/bni-credit-card"]
    Sleep   2s
    Input Text                      name=cardNumber                         5264221038874659
    Sleep   2s
    Click Element                   xpath=//*[@id="expMonth"]
    Sleep   2s
    Click Element                   xpath=//*[@id="expMonth"]/option[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="expYear"]
    Sleep   2s
    Click Element                   xpath=//*[@id="expYear"]/option[4]
    Sleep   2s
    Input Text                      name=cardCVV                                123
    Sleep   2s
    Click Element                   xpath=//*[@id="form-0-installment"]/div/form/div/div[2]/div/button
    Sleep   6s
    Select Frame                    xpath=//*[@id="cboxLoadedContent"]/iframe
    Sleep   2s
    Input Text                      css=input#PaRes                             112233
    Sleep   2s
    Click Element                   css=button.btn.btn-sm.btn-success
    Sleep   18s
    Wait Until Page Contains        Thank You for Your Purchase
    Sleep   1s
    Scroll Page To Location         0    1500
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   7s

Using Mandiri Installment Payment ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   10s
    Scroll Page To Location         0    1000
    Sleep   2s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div/a
    Sleep   7s
    Scroll Page To Location         0    500 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   22s
    Scroll Page To Location         0    750
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/installment-with-credit-card"]
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/installment-with-credit-card/mandiri-credit-card"]
    Sleep   2s
    Input Text                      name=cardNumber                         4617006959746656
    Sleep   2s
    Click Element                   xpath=//*[@id="expMonth"]
    Sleep   2s
    Click Element                   xpath=//*[@id="expMonth"]/option[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="expYear"]
    Sleep   2s
    Click Element                   xpath=//*[@id="expYear"]/option[4]
    Sleep   2s
    Input Text                      name=cardCVV                                123
    Sleep   2s
    Click Element                   xpath=//*[@id="form-0-installment"]/div/form/div/div[2]/div/button
    Sleep   6s
    Select Frame                    xpath=//*[@id="cboxLoadedContent"]/iframe
    Sleep   2s
    Input Text                      css=input#PaRes                             112233
    Sleep   2s
    Click Element                   css=button.btn.btn-sm.btn-success
    Sleep   18s
    Wait Until Page Contains        Thank You for Your Purchase
    Sleep   1s
    Scroll Page To Location         0    1500
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   7s

Using BCA Installment Payment ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   10s
    Scroll Page To Location         0    1000
    Sleep   2s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div/a
    Sleep   7s
    Scroll Page To Location         0    500 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   22s
    Scroll Page To Location         0    750
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/installment-with-credit-card"]
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/installment-with-credit-card/bca-credit-card"]
    Sleep   2s
    Input Text                      name=cardNumber                             4773776057051650
    Sleep   2s
    Click Element                   xpath=//*[@id="expMonth"]
    Sleep   2s
    Click Element                   xpath=//*[@id="expMonth"]/option[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="expYear"]
    Sleep   2s
    Click Element                   xpath=//*[@id="expYear"]/option[4]
    Sleep   2s
    Input Text                      name=cardCVV                                123
    Sleep   2s
    Click Element                   xpath=//*[@id="form-0-installment"]/div/form/div/div[2]/div/button
    Sleep   6s
    Select Frame                    xpath=//*[@id="cboxLoadedContent"]/iframe
    Sleep   2s
    Input Text                      css=input#PaRes                             112233
    Sleep   2s
    Click Element                   css=button.btn.btn-sm.btn-success
    Sleep   18s
    Wait Until Page Contains        Thank You for Your Purchase
    Sleep   1s
    Scroll Page To Location         0    1500
    Sleep   2s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   8s

Check Invoice BCA Transaction ID
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   6s
    Scroll Page To Location         0    750
    Sleep   2s
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[2]/table/tbody/tr[1]/td[7]/div
    Sleep   4s
    Select Window                   NEW
    Sleep   1s
    Close Window
    Sleep   1s
    Select Window                   title=Buy Packages - Bridestory for Business
    Sleep   1s

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute Javascript    window.scrollTo(${x_location},${y_location}) 


*** Test Cases ***
Test Installment Payment
    Launch Browser
    Login Vendor EN
    Using BNI Installment Payment EN
    Using Mandiri Installment Payment EN
    Using BCA Installment Payment EN
    Check Invoice BCA Transaction EN
    Log Out Vendor
    Switch Language
    Login Vendor ID
    Using BNI Installment Payment ID
    Using Mandiri Installment Payment ID
    Using BCA Installment Payment ID
    Check Invoice BCA Transaction ID
    Log Out Vendor