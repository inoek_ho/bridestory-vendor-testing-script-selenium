*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com/ 
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***


Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Switch Language
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div/div[3]/ul/li[3]/a/div[2]
    Sleep   2s

Login Vendor EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        testvendor1@mail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   14s

Vendor Verification EN
    Location Should Be              url=https://business-staging.bridestory.com/register/vendor#/register-vendor/5
    Sleep   1s
    Page Should Contain             CONGRATULATIONS!
    Page Should Contain             you are now a registered vendor at bridestory.com,
    Page Should Contain             but your profile has not been published yet...
    Page Should Contain             Complete the following steps to get your profile published at Bridestory.
    Page Should Contain             What do you want to do first?
    Page Should Contain             verify contact number
    Page Should Contain             Let’s make sure your future clients can reach you
    Page Should Contain             Add Profile Picture
    Page Should Contain             Put a face to your business and let your potential clients recognise you
    Page Should Contain             Add Projects
    Page Should Contain             With a minimum of 5 works
    Page Should Contain             Questions? 
    Page Should Contain             Help
    Page Should Contain             Skip this step and go to 
    Page Should Contain             My Dashboard
    Sleep   1s
    Click Element                   xpath=//*[@id="pg-phone"]/button
    Sleep   2s
    Wait Until Page Contains        Choose a phone number that you want to verify
    Sleep   2s
    Click Element                   xpath=/html/body/div[7]/div/div/div/div/div[1]/div[2]/div/form/button
    Sleep   2s
    Click Element                   xpath=/html/body/div[7]/div/div/div/a/i
    Sleep   2s
    Click Element                   xpath=//*[@id="pg-profile"]/a/button
    Sleep   2s
    Wait Until Page Contains        Add Profile Picture
    Page Should Contain Element     css=span.unpublish.ng-scope
    Sleep   2s
    Go Back
    Sleep   2s
    Click Element                   xpath=//*[@id="pg-project"]/a/button
    Sleep   2s
    Wait Until Page Contains        Before uploading your works,
    Page Should Contain Element     css=span.unpublish.ng-scope
    Sleep   2s
    Go Back
    Sleep   2s

Login Vendor ID
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        testvendor1@mail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   14s

Vendor Verification ID
    Location Should Be              url=https://business-staging.bridestory.com/id/register/vendor#/register-vendor/5
    Sleep   1s
    Page Should Contain             SELAMAT!
    Page Should Contain             Anda kini terdaftar sebagai vendor di bridestory.com
    Page Should Contain             tapi profil Anda belum dipublikasikan...
    Page Should Contain             Lengkapi langkah berikut agar profil Anda bisa dipublikasikan di Bridestory.
    Page Should Contain             Apa yang ingin Anda lakukan terlebih dulu?
    Page Should Contain             Verifikasi nomor kontak
    Page Should Contain             Pastikan calon klien mudah menghubungi Anda
    Page Should Contain             Gambar Profil
    Page Should Contain             Pasang wajah untuk bisnis Anda agar calon klien dapat mengenali Anda
    Page Should Contain             Tambah Album
    Page Should Contain             dengan minimal 5 karya
    Page Should Contain             Pertanyaan?
    Page Should Contain             Bantuan
    Page Should Contain             Ke Langkah Selanjutnya 
    Page Should Contain             Dashboard Saya
    Sleep   1s
    Click Element                   xpath=//*[@id="pg-phone"]/button
    Sleep   2s
    Wait Until Page Contains        Pilih nomor telepon yang ingin Anda verifikasi
    Sleep   2s
    Click Element                   xpath=/html/body/div[7]/div/div/div/div/div[1]/div[2]/div/form/button
    Sleep   2s
    Click Element                   xpath=/html/body/div[7]/div/div/div/a/i
    Sleep   2s
    Click Element                   xpath=//*[@id="pg-profile"]/a/button
    Sleep   2s
    Wait Until Page Contains        Gambar Profil
    Page Should Contain Element     css=span.unpublish.ng-scope
    Sleep   2s
    Go Back
    Sleep   2s
    Click Element                   xpath=//*[@id="pg-project"]/a/button
    Sleep   2s
    Wait Until Page Contains        Sebelum mengunggah hasil karya Anda
    Page Should Contain Element     css=span.unpublish.ng-scope
    Page Should Contain             coba lihat dulu panduan kami di bawah ini
    Sleep   2s
    Go Back
    Sleep   2s

Log Out Vendor
    Mouse Over                      xpath=//*[@id="sub-menu"]/li[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[8]/a
    Sleep   3s


*** Test Cases ***
Registrasi Vendor
    Launch Browser
    Login Vendor EN
    Vendor Verification EN
    Log Out Vendor
    Switch Language
    Login Vendor ID
    Vendor Verification ID
    Log Out Vendor