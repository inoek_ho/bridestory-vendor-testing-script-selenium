*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com/ 
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***
Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Switch Language
    Click Element   			xpath=//*[@id="sub-menu-not-login"]/div
    Click Element   			xpath=//*[@id="sub-menu-not-login"]/div/div[3]/ul/li[3]/a/div[2]
    Sleep   2s

Open Vendor Home From Bridestory User Staging
	Go To 						url=https://staging.bridestory.com
	Sleep	3s
	Click Element				css=a.register.link_vendor
	Sleep	3s
	Select Window				NEW
	Sleep	1s
	Location Should Contain 	https://business-staging.bridestory.com
	Close Window
	Sleep	1s
	Select Window 				title=Situs Pernikahan | Vendor Pernikahan dan Inspirasi | Bridestory
	Sleep	1s		
	Click Element				xpath=//*[@id="main"]/header/div[1]/div[3]/nav/a[2]/span[1]
	Sleep	3s
    Mouse Over                  css=.intercom-notification
    Sleep   2s
    Click Element               css=button.intercom-notifications-dismiss-button
    Sleep   2s
	Click Element				css=a.btn.btn-grey.btn-block.mt-30
	Sleep	2s

Reg Step 1 EN
    Click Element   			css=a.hp_vendor_login
    Sleep   1s
    Click Element				css=a.brandon.f-c-green-strong
    Sleep	2s
    Wait Until Page Contains    VENDOR REGISTRATION
    Sleep   2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain         Your name cannot be empty
    Page Should Contain 		Your mobile area code cannot be empty
    Page Should Contain 		Your mobile number cannot be empty
    Page Should Contain        	This email address cannot be empty
    Page Should Contain        	Your password cannot be empty
    Page Should Contain 		Confirm password cannot be empty
    Sleep	1s
    Input Text      			name=full_name      Yoshua T
    Sleep	2s
    Select From List By Value   xpath=/html/body/div[2]/section/div/div[5]/form/div[2]/div/select         92
    Sleep	2s
    Input Text     				name=mobile_phone       85634243546
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[5]/form/div[3]/div[4]/div[1]/label/span
    Sleep	2s
    Input Text      			${EMAIL}        iniemailvendor
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		This email address is invalid
    Sleep	2s
    Input Text      			${EMAIL}        autotestvendor@mail.com
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		This email address is already registered
    Sleep	2s
    Input Text      			${EMAIL}        skyfallwedding@mail.co
    Sleep	2s
    Input Password  			${PASSWORD}       123
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		Your password cannot be less than 6 charaters
    Sleep	2s
    Input Password  			${PASSWORD}       123456
    Sleep	2s
    Input Password  			name=confirm_password       123
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		Your password not match
    Sleep	2s
    Input Password  			name=confirm_password       123456
    Sleep	2s
    Click Element				css=button.btn.with-border.w-130.right
    Sleep	2s

Reg Step 2 EN
    Wait Until Page Contains    Business Category
    Sleep   2s
    Location Should Be 			url=https://business-staging.bridestory.com/register/vendor#/register-vendor/2
    Sleep	1s
    Click Element				css=button.btn.btn-link.btn-back
    Sleep	2s
    Wait Until Page Contains    VENDOR REGISTRATION
    Sleep   1s
    Click Element				css=button.btn.with-border.w-130.right
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep 	2s
    Page Should Contain 		Business category required
    Page Should Contain 		Business name required
	Sleep	1s
	Click Element 				xpath=/html/body/div[2]/section/div/div[5]/form/div[1]/label/div/select
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[5]/form/div[1]/label/div/select/option[11]
    Sleep	2s
    Input Text					name=business_name		Skyfall Wedding Organizer
    Sleep	2s
    Press Key					name=city				\\8
    Sleep 	1s
    Press Key					name=city				\\8
    Sleep 	1s 
    Press Key					name=city				\\8
    Sleep 	1s 
    Press Key					name=city				\\8
    Sleep 	1s 
    Press Key					name=city				\\8
    Sleep 	1s 
    Press Key					name=city				\\8
    Sleep 	1s 
    Press Key					name=city				\\8
    Sleep 	1s 
    Press Key					name=city				\\8
    Sleep 	1s 
    Press Key					name=city				\\8
    Sleep 	1s 
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		Country & city required
    Sleep	1s
    Input Text					name=city				Bekais
    Sleep 	2s 
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		City not registered
    Sleep	1s
    Input Text					name=city				Bekasi
    Sleep	2s
    Input Text					name=post_code			11530
    Sleep	2s
    Input Text					name=address			Jl. Budi Raya No.21
    Sleep	2s
    Input Text					name=website			Weddingkuuuu
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		Not a valid website url
    Sleep	1s
    Input Text					name=website			www.makeyourwedding.com
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s

Reg Step 3 EN
    Wait Until Page Contains	How do you define your product by price?
    Sleep 	1s
    Location Should Be 			url=https://business-staging.bridestory.com/register/vendor#/register-vendor/3
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		Please select an answer
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/div[5]/div/form/ng-switch/section/div/div/label[1]/span
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep 	2s
    Wait Until Page Contains	How do you know about Bridestory?
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		Please select an answer
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/div[5]/div/form/ng-switch/section/div/div[1]/label[3]/span
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[5]/div/form/ng-switch/section/div/div[1]/label[3]/div/div/select/option[3]
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/a/button
    Sleep	2s
    Location Should Be 			url=https://business-staging.bridestory.com/register/vendor#/register-vendor/2
    Sleep	1s
    Click Element               xpath=/html/body/div[2]/section/div/div[5]/form/div[1]/label/div/select
    Sleep   2s
    Click Element               xpath=/html/body/div[2]/section/div/div[5]/form/div[1]/label/div/select/option[11]
    Sleep   2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Wait Until Page Contains	How do you define your product by price?
    Sleep 	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		Please select an answer
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/div[5]/div/form/ng-switch/section/div/div/label[1]/span
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep 	2s
    Wait Until Page Contains	How do you know about Bridestory?
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		Please select an answer
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/div[5]/div/form/ng-switch/section/div/div[1]/label[7]/span
    Sleep	2s
    Page Should Contain 		Please specify your answer
    Sleep	1s
    Input Text					xpath=/html/body/div[2]/section/div/div[5]/div/form/ng-switch/section/div/div[1]/label[7]/div/textarea 			Tiba-tiba tau aja gitu
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s

Reg Step 4 EN
    Wait Until Page Contains	DATA SUMMARY
    Sleep 	2s
    Location Should Be 			url=https://business-staging.bridestory.com/register/vendor#/register-vendor/4
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/section[1]/div[1]/a/button
    Sleep	2s
    Location Should Be 			url=https://business-staging.bridestory.com/register/vendor#/register-vendor
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[2]
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/section[2]/div[1]/a/button
    Sleep	2s
    Location Should Be 			url=https://business-staging.bridestory.com/register/vendor#/register-vendor/2
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[2]
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/section[3]/div[1]/a/button
    Sleep	2s
    Location Should Be 			url=https://business-staging.bridestory.com/register/vendor#/register-vendor/3
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[2]
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[2]
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/section[4]/div[1]/label/div/a
    Sleep	2s
    Select Window				title=NEW
    Sleep	2s
    Wait Until Page Contains	Terms and Conditions
    Sleep	2s
    Close Window
    Sleep	2s
    Select Window				title=Wedding Vendors - Bridestory.com
    Sleep	2s
    Page Should Contain Element	css=button.btn.with-border.btn-small.btn-short.right.is-disabled
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/section[4]/div[1]/label/span
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/section[4]/div[3]/button
    Sleep	2s
    Wait Until Page Contains	but your profile has not been published yet...
    Sleep	2s
    Location Should Be 			url=https://business-staging.bridestory.com/register/vendor#/register-vendor/5
    Sleep	1s

Log Out Vendor
	Mouse Over					css=li.list-menu.dropdown
	Sleep	2s
	Click Element				xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[8]/a/div[2]
	Sleep	4s

Reg Step 1 ID
    Click Element   			xpath=/html/body/div[4]/div[2]/a/button
    Sleep   2s
    Wait Until Page Contains    PENDAFTARAN Vendor
    Sleep   2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain         Nama Anda harus diisi
    Page Should Contain 		Nomor telepon seluler Anda harus diisi
    Page Should Contain 		Alamat email harus diisi
    Page Should Contain        	Kata sandi Anda harus diisi
    Page Should Contain 		Konfirmasi kata sandi tidak boleh dikosongkan
    Sleep	1s
    Input Text      			name=full_name      Vina Navi
    Sleep	2s
    Select From List By Value   xpath=/html/body/div[2]/section/div/div[5]/form/div[2]/div/select         92
    Sleep	2s
    Input Text     				name=mobile_phone       81344389067
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[5]/form/div[3]/div[4]/div[1]/label/span
    Sleep	2s
    Input Text      			${EMAIL}        emailuntukcontact
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		Alamat email tidak valid
    Sleep	2s
    Input Text      			${EMAIL}        autotestvendor@mail.com
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		Alamat email sudah terdaftar
    Sleep	2s
    Input Text      			${EMAIL}        skydreaming@mail.co
    Sleep	2s
    Input Password  			${PASSWORD}       123
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		Kata sandi Anda tidak bisa kurang dari 6 karakter
    Sleep	2s
    Input Password  			${PASSWORD}       123456
    Sleep	2s
    Input Password  			name=confirm_password       123
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		Kata sandi Anda tidak cocok
    Sleep	2s
    Input Password  			name=confirm_password       123456
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s

Reg Step 2 ID
    Wait Until Page Contains    Kategori Bisnis
    Sleep   2s
    Location Should Be 			url=https://business-staging.bridestory.com/id/register/vendor#/register-vendor/2
    Sleep	1s
    Click Element				css=button.btn.btn-link.btn-back
    Sleep	2s
    Wait Until Page Contains    VENDOR REGISTRATION
    Sleep   1s
    Click Element				css=button.btn.with-border.w-130.right
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep 	2s
    Page Should Contain 		Kategori bisnis harus dicantumkan
    Page Should Contain 		Nama bisnis harus diisi
	Sleep	1s
	Click Element 				xpath=/html/body/div[2]/section/div/div[5]/form/div[1]/label/div/select
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[5]/form/div[1]/label/div/select/option[11]
    Sleep	2s
    Input Text					name=business_name		Sky Dreaming
    Sleep	2s
    Press Key					name=city				\\8
    Sleep 	1s
    Press Key					name=city				\\8
    Sleep 	1s 
    Press Key					name=city				\\8
    Sleep 	1s 
    Press Key					name=city				\\8
    Sleep 	1s 
    Press Key					name=city				\\8
    Sleep 	1s 
    Press Key					name=city				\\8
    Sleep 	1s 
    Press Key					name=city				\\8
    Sleep 	1s 
    Press Key					name=city				\\8
    Sleep 	1s 
    Press Key					name=city				\\8
    Sleep 	1s 
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		Negara dan kota harus diisi
    Sleep	1s
    Input Text					name=city				Samarnda
    Sleep 	2s 
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		Kota tidak terdaftar
    Sleep	1s
    Input Text					name=city				Samarinda
    Sleep	2s
    Input Text					name=post_code			11530
    Sleep	2s
    Input Text					name=address			Medan Raya No.21
    Sleep	2s
    Input Text					name=website			Skydreming
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		URL situs tidak valid
    Sleep	1s
    Input Text					name=website			www.skydreaming.com
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s

Reg Step 3 ID
    Wait Until Page Contains	Sebelum Anda menarik perhatian klien potensial, kami ingin memastikan bahwa bisnis Anda terdaftar secara akurat.
    Sleep 	1s
    Location Should Be 			url=https://business-staging.bridestory.com/id/register/vendor#/register-vendor/3
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		Mohon pilih jawaban
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/div[5]/div/form/ng-switch/section/div/div/label[1]/span
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep 	2s
    Wait Until Page Contains	Dari mana Anda tahu soal Bridestory?
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		Mohon pilih jawaban
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/div[5]/div/form/ng-switch/section/div/div[1]/label[3]/span
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[5]/div/form/ng-switch/section/div/div[1]/label[3]/div/div/select/option[3]
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/a/button
    Sleep	2s
    Location Should Be 			url=https://business-staging.bridestory.com/id/register/vendor#/register-vendor/2
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Wait Until Page Contains	Sebelum Anda menarik perhatian klien potensial, kami ingin memastikan bahwa bisnis Anda terdaftar secara akurat.
    Sleep 	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		Mohon pilih jawaban
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/div[5]/div/form/ng-switch/section/div/div/label[1]/span
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep 	2s
    Wait Until Page Contains	Dari mana Anda tahu soal Bridestory?
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s
    Page Should Contain 		Mohon pilih jawaban
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/div[5]/div/form/ng-switch/section/div/div[1]/label[7]/span
    Sleep	2s
    Page Should Contain 		Mohon jelaskan jawaban Anda 
    Sleep	1s
    Input Text					xpath=/html/body/div[2]/section/div/div[5]/div/form/ng-switch/section/div/div[1]/label[7]/div/textarea 			Tiba-tiba tau aja gitu
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[1]
    Sleep	2s

Reg Step 4 ID
    Wait Until Page Contains	RINGKASAN DATA
    Sleep 	2s
    Location Should Be 			url=https://business-staging.bridestory.com/id/register/vendor#/register-vendor/4
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/section[1]/div[1]/a/button
    Sleep	2s
    Location Should Be 			url=https://business-staging.bridestory.com/id/register/vendor#/register-vendor
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[2]
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/section[2]/div[1]/a/button
    Sleep	2s
    Location Should Be 			url=https://business-staging.bridestory.com/id/register/vendor#/register-vendor/2
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[2]
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/section[3]/div[1]/a/button
    Sleep	2s
    Location Should Be 			url=https://business-staging.bridestory.com/id/register/vendor#/register-vendor/3
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[2]
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/div[6]/button[2]
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/section[4]/div[1]/label/div/a
    Sleep	2s
    Select Window				NEW
    Sleep	2s
    Wait Until Page Contains	Syarat dan Ketentuan
    Sleep	2s
    Close Window
    Sleep	2s
    Select Window				title=Wedding Vendors - Bridestory.com
    Sleep	2s
    Page Should Contain Element	css=button.btn.with-border.btn-small.btn-short.right.is-disabled
    Sleep	1s
    Click Element				xpath=/html/body/div[2]/section/div/section[4]/div[1]/label/span
    Sleep	2s
    Click Element				xpath=/html/body/div[2]/section/div/section[4]/div[3]/button
    Sleep	2s
    Wait Until Page Contains	tapi profil Anda belum dipublikasikan...
    Sleep	2s
    Location Should Be 			url=https://business-staging.bridestory.com/id/register/vendor#/register-vendor/5
    Sleep	1s

Verify Business Blog
    Click Element				xpath=//*[@id="header2"]/div/div/div/div[2]/a/div
    Sleep	5s
    Click Element				xpath=/html/body/div[2]/section/article[1]/h2/a
    Sleep	6s
    Go Back 
    Click Element				xpath=/html/body/div[2]/section/article[1]/a[2]/img
    Sleep	6s
    Go Back
    Click Element				xpath=/html/body/div[2]/section/article/a
    Sleep	6s


*** Test Cases ***
Registrasi Vendor
    Launch Browser

    Reg Step 1 EN
    Reg Step 2 EN
    Reg Step 3 EN
    Reg Step 4 EN
    Log Out Vendor
    Switch Language
    Reg Step 1 ID
    Reg Step 2 ID
    Reg Step 3 ID
    Reg Step 4 ID
    Log Out Vendor
    Verify Business Blog