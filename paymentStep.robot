*** Settings ***
Library           Selenium2Library

*** Variables***
${Browser}		gc
${BUSINESS}     https://business-staging.bridestory.com/ 
${EMAIL}        name=email
${PASSWORD}     name=password 

*** Keyword ***
Launch Browser
    Open Browser    ${BUSINESS}       ${Browser}
    Maximize Browser Window
    Sleep    2s

Switch Language
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/div/div[3]/ul/li[3]/a/div[2]
    Sleep   2s

Login Vendor EN
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        francodafer@gmail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   14s

Payment Using Credit Card EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   7s
    Scroll Page To Location         0    900
    Sleep   2s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div/a
    Sleep   6s
    Page Should Contain Element		css=.right.w-310.mt-30.box.no-padding.with-border.checkout-summary.ng-scope
    Sleep	1s
    Scroll Page To Location         0    1500 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   24s
    Page Should Contain Element		css=.bsp-summary-items
    Page Should Contain	Element		css=.bsp-summary-box
    Page Should Contain Element		css=.redeem-check
    Sleep	1s
    Click Element					xpath=/html/body/ng-view/div/div[1]/label/div[1]/label/div
    Sleep	12s
    Page Should Contain Element		xpath=/html/body/ng-view/div/div[1]/div[2]/div[2]
    Sleep	1s
    Click Element					xpath=/html/body/ng-view/div/div[1]/label/div[1]/label/div
    Sleep	12s
    Scroll Page To Location         0    750
    Sleep   1s
    Page Should Contain Element 	css=.bsp-summary-earn.ng-scope
    Sleep	1s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Page Should Contain Element		xpath=//a[@href="/payment/credit-card"]
    Page Should Contain Element		xpath=//a[@href="/payment/bank-transfer"]
    Page Should Contain Element		xpath=//a[@href="/payment/installment-with-credit-card"]
    Sleep	1s
    Click Element                   xpath=//a[@href="/payment/bank-transfer"]
    Sleep	2s
    Click Element                   xpath=//a[@href="/payment/installment-with-credit-card"]
    Sleep	2s
    Click Element                   xpath=//a[@href="/payment/credit-card"]
    Sleep   2s
	Input Text                      name=cardNumber                             5211111111111117
    Sleep   2s
    Click Element                   xpath=//*[@id="expMonth"]
    Sleep   2s
    Click Element                   xpath=//*[@id="expMonth"]/option[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="expYear"]
    Sleep   2s
    Click Element                   xpath=//*[@id="expYear"]/option[4]
    Sleep   2s
    Input Text                      name=cardCVV                                123
    Sleep   2s
    Click Element                   xpath=//*[@id="form-credit-card"]/div[2]/div/button
    Sleep 	21s
    Wait Until Page Contains		Thank You for Your Purchase
    Sleep	1s
    Scroll Page To Location         0    1200
    Sleep   1s
    Click Element					xpath=/html/body/ng-view/div/div[2]/button
    Sleep	10s
	Page Should Contain Element		css=.box.with-border.wrapper.center.mt-50.credits__exchange-discount.ng-scope
	Sleep	1s

 Payment Using Bank Transfer EN
 	Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   10s
    Scroll Page To Location         0    900
    Sleep   2s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div
    Sleep   6s
    Page Should Contain Element		css=.right.w-310.mt-30.box.no-padding.with-border.checkout-summary.ng-scope
    Sleep	1s
    Scroll Page To Location         0    1500 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   15s
    Page Should Contain Element		css=.bsp-summary-items
    Page Should Contain	Element		css=.bsp-summary-box
    Page Should Contain Element		css=.redeem-check
    Sleep	1s
    Click Element					xpath=/html/body/ng-view/div/div[1]/label/div[1]/label/div
    Sleep	7s
    Page Should Contain Element		xpath=/html/body/ng-view/div/div[1]/div[2]/div[2]
    Sleep	1s
    Scroll Page To Location         0    750
    Sleep   1s
    Page Should Contain Element 	css=.bsp-summary-earn.ng-scope
    Sleep	1s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Page Should Contain Element		xpath=//a[@href="/payment/credit-card"]
    Page Should Contain Element		xpath=//a[@href="/payment/bank-transfer"]
    Click Element                   xpath=//a[@href="/payment/credit-card"]
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/bank-transfer"]
    Sleep	2s
    Click Element					xpath=//*[@id="form-bank-transfer"]/div[1]/div[2]/div/ul/li[1]/label
    Sleep	1s
    Click Element					xpath=//*[@id="form-bank-transfer"]/div[2]/div/button
    Sleep	22s
    Wait Until Page Contains		Thank You for Your Purchase
    Sleep	1s
    Scroll Page To Location         0    1200
    Sleep   1s
    Click Element					xpath=/html/body/ng-view/div/div[2]/button
    Sleep	10s

View Invoice With BCA VA Number EN
    Scroll Page To Location         0    550
    Sleep   2s
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[1]/table/tbody/tr/td[7]/div[2]/a/i
    Sleep   6s
    Select Window                   NEW
    Sleep   2s
    Scroll Page To Location         0    850
    Sleep   2s
    Page Should Contain             Transfer to BCA Virtual Account”
    Sleep   2s
    Page Should Contain             Please enter the Virtual Account number below:
    Sleep   2s
    Close Window
    Sleep   2s
    Select Window                   title=Buy Packages - Bridestory for Business
    Sleep   2s

View Vendor Transaction EN
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   8s
    Wait Until Page Contains        Pending Transaction
    Sleep   1s
    Wait Until Page Contains        Transaction History
    Sleep   4s

Cancel Transaction EN
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[1]/table/tbody/tr/td[7]/div[1]/i
    Sleep   2s
    Wait Until Page Contains        Are you sure want to cancel this transaction?
    Sleep   1s
    Click Element                   css=button.btn.btn-stronk.with-border.mw-150.ng-binding
    Sleep   2s
   
Log Out Vendor
    Mouse Over                      css=li.list-menu.dropdown
    Sleep   2s
    Click Element                   xpath=//*[@id="sub-menu"]/li[2]/div[2]/div/div[9]/a/div[2]
    Sleep   5s

Login Vendor ID
    Click Element                   xpath=//*[@id="sub-menu-not-login"]/a
    Sleep   2s
    Input Text                      ${EMAIL}        francodafer@gmail.com
    Sleep   2s
    Input Text                      ${PASSWORD}     123456
    Sleep   2s
    Click Element                   xpath=/html/body/div[17]/div/div/div/div[1]/ng-switch/section/div[1]/form/button
    Sleep   14s

Payment Using Credit Card ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   7s
    Scroll Page To Location         0    900
    Sleep   2s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div/a
    Sleep   6s
    Page Should Contain Element     css=.right.w-310.mt-30.box.no-padding.with-border.checkout-summary.ng-scope
    Sleep   1s
    Scroll Page To Location         0    1500 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   15s
    Page Should Contain Element     css=.bsp-summary-items
    Page Should Contain Element     css=.bsp-summary-box
    Page Should Contain Element     css=.redeem-check
    Sleep   1s
    Click Element                   xpath=/html/body/ng-view/div/div[1]/label/div[1]/label/div
    Sleep   7s
    Page Should Contain Element     xpath=/html/body/ng-view/div/div[1]/div[2]/div[2]
    Sleep   1s
    Click Element                   xpath=/html/body/ng-view/div/div[1]/label/div[1]/label/div
    Sleep   7s
    Scroll Page To Location         0    750
    Sleep   1s
    Page Should Contain Element     css=.bsp-summary-earn.ng-scope
    Sleep   1s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Page Should Contain Element     xpath=//a[@href="/payment/credit-card"]
    Page Should Contain Element     xpath=//a[@href="/payment/bank-transfer"]
    Page Should Contain Element     xpath=//a[@href="/payment/installment-with-credit-card"]
    Sleep   1s
    Click Element                   xpath=//a[@href="/payment/bank-transfer"]
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/installment-with-credit-card"]
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/credit-card"]
    Sleep   2s
    Input Text                      name=cardNumber                             5211111111111117
    Sleep   2s
    Click Element                   xpath=//*[@id="expMonth"]
    Sleep   2s
    Click Element                   xpath=//*[@id="expMonth"]/option[2]
    Sleep   2s
    Click Element                   xpath=//*[@id="expYear"]
    Sleep   2s
    Click Element                   xpath=//*[@id="expYear"]/option[4]
    Sleep   2s
    Input Text                      name=cardCVV                                123
    Sleep   2s
    Click Element                   xpath=//*[@id="form-credit-card"]/div[2]/div/button
    Sleep   21s
    Wait Until Page Contains        Thank You for Your Purchase
    Sleep   1s
    Scroll Page To Location         0    1200
    Sleep   1s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   10s
    Page Should Contain Element     css=.box.with-border.wrapper.center.mt-50.credits__exchange-discount.ng-scope
    Sleep   1s

 Payment Using Bank Transfer ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   10s
    Scroll Page To Location         0    900
    Sleep   2s
    Click Element                   xpath=//*[@id="buyplan"]/div[1]/table/thead/tr/th[2]/div[4]/div/a
    Sleep   6s
    Page Should Contain Element     css=.right.w-310.mt-30.box.no-padding.with-border.checkout-summary.ng-scope
    Sleep   1s
    Scroll Page To Location         0    1500 
    Sleep   2s
    Click Element                   css=a.btn.btn-block.with-border.mt-10.tracker_buttoncheckout
    Sleep   24s
    Page Should Contain Element     css=.bsp-summary-items
    Page Should Contain Element     css=.bsp-summary-box
    Page Should Contain Element     css=.redeem-check
    Sleep   1s
    Click Element                   xpath=/html/body/ng-view/div/div[1]/label/div[1]/label/div
    Sleep   12s
    Page Should Contain Element     xpath=/html/body/ng-view/div/div[1]/div[2]/div[2]
    Sleep   1s
    Scroll Page To Location         0    750
    Sleep   1s
    Page Should Contain Element     css=.bsp-summary-earn.ng-scope
    Sleep   1s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/a/button
    Sleep   2s
    Page Should Contain Element     xpath=//a[@href="/payment/credit-card"]
    Page Should Contain Element     xpath=//a[@href="/payment/bank-transfer"]
    Click Element                   xpath=//a[@href="/payment/credit-card"]
    Sleep   2s
    Click Element                   xpath=//a[@href="/payment/bank-transfer"]
    Sleep   2s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[1]/div[2]/div/ul/li[1]/label
    Sleep   1s
    Click Element                   xpath=//*[@id="form-bank-transfer"]/div[2]/div/button
    Sleep   22s
    Wait Until Page Contains        Thank You for Your Purchase
    Sleep   1s
    Scroll Page To Location         0    1200
    Sleep   1s
    Click Element                   xpath=/html/body/ng-view/div/div[2]/button
    Sleep   10s

View Invoice With BCA VA Number ID
    Scroll Page To Location         0    550
    Sleep   2s
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[1]/table/tbody/tr/td[7]/div[2]/a/i
    Sleep   6s
    Select Window                   NEW
    Sleep   2s
    Scroll Page To Location         0    850
    Sleep   2s
    Page Should Contain             "Transfer ke Rek. BCA Virtual Account"
    Sleep   2s
    Page Should Contain             Silakan masukkan nomor Virtual Account:
    Sleep   2s
    Close Window
    Sleep   2s
    Select Window                   title=Buy Packages - Bridestory for Business
    Sleep   2s

View Vendor Transaction ID
    Click Element                   xpath=//*[@id="subheader-dashboard"]/ul/li[4]/a
    Sleep   5s
    Click Element                   xpath=//*[@id="dashboard-pagesub2"]/div/ul/li[2]/a
    Sleep   8s
    Wait Until Page Contains        Transaksi yang Tertunda
    Sleep   1s
    Wait Until Page Contains        Transaction History
    Sleep   4s

Cancel Transaction ID
    Click Element                   xpath=//*[@id="topmost-wrapper"]/div[1]/table/tbody/tr/td[7]/div[1]/i
    Sleep   2s
    Wait Until Page Contains        Apakah Anda yakin ingin membatalkan transaksi ini?
    Sleep   1s
    Click Element                   css=button.btn.btn-stronk.with-border.mw-150.ng-binding
    Sleep   2s

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute Javascript    window.scrollTo(${x_location},${y_location}) 


*** Test Cases ***
Test Installment Payment
    Launch Browser
    Login Vendor EN 
    Payment Using Credit Card EN 
    Payment Using Bank Transfer EN
    View Vendor Transaction EN
    View Invoice With BCA VA Number EN
    Cancel Transaction EN
    Log Out Vendor
    Switch Language
    Login Vendor ID
    Payment Using Credit Card ID
    Payment Using Bank Transfer ID
    View Vendor Transaction ID
    View Invoice With BCA VA Number ID
    Cancel Transaction ID
    Log Out Vendor